import React, {Component} from 'react';

import {Segment, Label, Grid} from 'semantic-ui-react';
import StatusMessage from '../statusmessage';
import './styles.css';

export default class CredentialList extends Component {
  // onDelete = (id) => {
  //     const {deleteCredential} = this.props;
  //     deleteCredential(id);
  // }

  render() {
    // const {isLoading, error, credentials} = this.props;
    const {isLoading, error, credentials, deleteCredential} = this.props;

    if (error || !credentials || isLoading || credentials.length === 0) {
      return (
        <StatusMessage
          error={error || !credentials}
          errorClassName="credential-error"
          errorMessage={error}
          loading={isLoading}
          loadingMessage={'We are fetching the credentials for you'}
          nothing={credentials && credentials.length === 0}
          nothingMessage={'No credential to display'}
          nothingClassName="credential-error"
          type="default"
        />
      );
    }

    // const credentials = [
    //     {
    //         id:1,
    //         name: "c1",
    //         username: "u1",
    //         token: "t1"
    //     },{
    //         id:2,
    //         name: "c2",
    //         username: "u2",
    //         token: "t2"
    //     },
    // ]

    const credentialList = credentials.map(credential => {
      let {id, name, username, token, cate, color} = credential;
      return (
        <Segment vertical key={id}>
          <Label as="a" color={color} ribbon="right">
            {cate}
          </Label>
          <Grid textAlign="left" padded="horizontally">
            <Grid.Column width={4}>{name}</Grid.Column>
            <Grid.Column width={4}>{username}</Grid.Column>
            <Grid.Column width={4}>{token}</Grid.Column>
            <Grid.Column width={2}>修改</Grid.Column>
            {/*<Grid.Column width={2}><div onClick={() => this.onDelete(id) }>删除</div></Grid.Column>*/}
            <Grid.Column width={2}>
              <label onClick={() => deleteCredential(id)}>删除</label>
            </Grid.Column>
          </Grid>
        </Segment>
      );
    });

    return (
      <div className="credentialContainer">
        <Segment.Group className="credential-list">
          {credentialList}
        </Segment.Group>
      </div>
    );
  }
}
