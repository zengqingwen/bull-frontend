import {
  FETCH_CATEGORY_REQUEST,
  FETCH_CATEGORY_SUCCESS,
  FETCH_CATEGORY_FAILURE,
} from './types';
import {fetchCategoryApi} from '../api';
import {apiErrorHandler} from '../utils/errorhandler';

export const fetchCategory = () => dispatch => {
  dispatch(fetchCategoryRequest());

  fetchCategoryApi()
    .then(response => {
      dispatch(fetchCategorySuccess(response.data));
    })
    .catch(error => {
      const errorMessage = apiErrorHandler(error);
      dispatch(fetchCategoryFailure(errorMessage));
    });
};

export const fetchCategoryRequest = () => {
  return {
    type: FETCH_CATEGORY_REQUEST,
  };
};

export const fetchCategorySuccess = category => {
  return {
    type: FETCH_CATEGORY_SUCCESS,
    categories: category,
  };
};

export const fetchCategoryFailure = error => {
  return {
    type: FETCH_CATEGORY_FAILURE,
    error,
  };
};
