import React, {Component} from 'react';
import {connect} from 'react-redux';
import {Link} from 'react-router-dom';

import {Button, Grid, Header, Menu, Input, Dropdown} from 'semantic-ui-react';

import {fetchProjects} from '../../actions';

class NewProject extends Component {
  // state = {
  //   activeItem: 'Your projects',
  //   scop: 'yourproject',
  //   keyword: '',
  //   sort: '',
  // };

  handleItemClick = (e, {scop}) => {
    const {scop: scopOld, keyword, sort} = this.props
    const oldParam = {scop: scopOld, keyword, sort}
    this.props.changeFilterScop({...oldParam, scop, scopInit: false,});
    // this.setState({activeItem: name})
    // this.setState({scop: scop})
    // // let {activeItem, ...query_args} = this.state;
    // // this.props.fetchProjectsWithParms(query_args);
  };

  handleChange = (e, {name, value}) => {
    const {scop, keyword, sort} = this.props
    const oldParam = {scop, keyword, sort}
    this.props.changeFilterScop({...oldParam, [name]: value, scopInit: false,});
    // this.setState({[name]: value});
  };

  componentWillReceiveProps(newProps) {
    const {scop, keyword, sort} = newProps
    const {scop: scopOld, keyword: keywordOld, sort: sortOld} = this.props
    const newParam = {scop, keyword, sort}
    const oldParam = {scop: scopOld, keyword: keywordOld, sort: sortOld}
    if (newParam !== oldParam) {
      // if(!this.props.scopInit) {        }    // fuck
      if(!newProps.scopInit) {
        // console.log("###componentWillReceiveProps123")
        // console.log(newParam)
        // console.log(oldParam)
        this.props.fetchProjectsWithParms(newParam);
      }
    }
  }

  /*****
   * 记录一个超级恶心的问题：
   * 问题起因：本想每次改变筛选条件，就可以重新触发一次获取projects的筛选后的新props。
   * 但是componentDidUpdate无法执行从父组件（src/containers/home/index.js）传来的异步函数（this.props.fetchProjects()。　
   *    refer to: https://www.jianshu.com/p/713206e571cf  react 子组件获取父组件传来的 props 问题
   *              https://www.jianshu.com/p/65c50b7418af  react中父组件异步修改参数，传递给子组件时遇到的问题　
   *                ×【原来父组件传递的参数改变后，子组件会重新渲染的只有render函数，所以将console.log(’testParam：‘, this.props.testParam)放在子组件的render函数中，发现可以获取到父组件传来的值了。】
   * 本来componentWillReceiveProps　函数中可以执行以上的函数，但是本来componentWillReceiveProps周期函数只能监听到props的变化（props变化才会执行）。但是我就是想把这些放在state中。继续纠结...
   *
   * 那么既然不能执行传来的，就只好在该子组件中重新导入fetchProjects异步函数（我把他们区别命名为fetchProjectsWithParms）
   * 父组件中的fetchProjects()不需要带参数；然鹅本子组件需要带参数fetchProjectsWithParms(query_args)，故而(src/actions/home.js 下的fetchProjects可以接一个可变参数)
   * then: 问题搞定！！！
   *
   * 还有如果fetchProjectsWithParms(query_args)放在handleItemClick中执行，会延迟一步获取最新的state，所以只好放在componentDidUpdate周期函数中
   */
  // componentDidUpdate(prevProps, prevState) {
  //   // Callbacks (side effects) are only safe after commit.
  //   // if (this.state.someStatefulValue !== prevState.someStatefulValue) {
  //   //   this.props.onChange(this.state.someStatefulValue);
  //   // }
  //   var {scop, keyword, sort} = this.state;
  //   if(scop || keyword || sort) {
  //
  //     let {activeItem, ...query_args} = this.state;
  //     // console.log(query_args)
  //     this.props.fetchProjectsWithParms(query_args);
  //   }
  // }

  render() {
    // const {activeItem, keyword, sort} = this.state;
    const {activeItem, keyword, sort} = this.props;

    const sortBy = [
      // {
      //     key: 'sort',
      //     text: (
      //         <span>
      //             You may want to <strong>sort by</strong>
      //         </span>
      //     ),
      //     disabled: true,
      // },
      {key: 'lu', text: 'Last update', value: 'updated_at'},
      {key: 'ou', text: 'Oldest update', value: '-updated_at'},
      {key: 'lc', text: 'Last created', value: 'created_at'},
      {key: 'oc', text: 'Oldest created', value: '-created_at'},
      {key: 'na', text: 'Name Asc', value: 'name'},
      {key: 'nd', text: 'Name Desc', value: '-name'},
    ];
    return (
      <div>
        <Header as="h2" dividing>
          Menu
        </Header>

        <Grid columns={1} doubling>
          <Grid.Column>
            {/*<Menu*/}
            {/*items={[*/}
            {/*{ key: '1', name: 'link-1', content: 'Link' },*/}
            {/*{ key: '2', name: 'link-2', content: 'Link' },*/}
            {/*{ key: '3', name: 'link-3', content: 'Link' },*/}
            {/*]}*/}
            {/*pointing*/}
            {/*secondary*/}
            {/*/>*/}

            <Menu secondary pointing>
              <Menu.Menu>
                <Menu.Item
                  name="Your projects"
                  active={activeItem === 'yourproject'}
                  onClick={this.handleItemClick}
                  scop='yourproject'
                />
                <Menu.Item
                  name="Starred projects"
                  active={activeItem === 'starproject'}
                  onClick={this.handleItemClick}
                  scop='starproject'
                />
                <Menu.Item
                  name="All projects"
                  active={activeItem === 'allproject'}
                  onClick={this.handleItemClick}
                  scop='allproject'
                />
              </Menu.Menu>

              <Menu.Menu position="right">
                <Menu.Item>
                  <Input
                    icon="search"
                    placeholder="Search..."
                    name="keyword"
                    value={keyword}
                    onChange={this.handleChange}
                  />
                </Menu.Item>
                <Menu.Item>
                  <Dropdown
                    placeholder="Sort by"
                    // style={{height: '2em'}}
                    selection
                    options={sortBy}
                    name="sort"
                    value={sort}
                    onChange={this.handleChange}
                  />
                </Menu.Item>
                <Menu.Item>
                  <Link to={`/projects/new`}>
                    <Button color="green">New Project</Button>
                  </Link>
                </Menu.Item>
              </Menu.Menu>
            </Menu>
          </Grid.Column>
        </Grid>
      </div>
    );
  }
}

const mapDispatchToProps = dispatch => ({
  fetchProjectsWithParms: (...args) => {
    dispatch(fetchProjects(...args));
  },
});

export default connect(null, mapDispatchToProps)(NewProject);