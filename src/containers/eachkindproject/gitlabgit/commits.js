import React, {Component} from 'react';
// import {connect} from 'react-redux';
import {Segment} from 'semantic-ui-react';

import CommitsForm from './components/commitsform';
import CommitsTable from './components/commitstable';

// import {
//   fetchGitCommits,
//   fetchGitBranchesTags,
// } from '../../../actions';

import './styles.css';

class Commits extends Component {
  render() {
    // console.log("2222222222222222222")
    // console.log(this.props)
    // console.log(this.props.location)
    // console.log(this.props.location.state.projectinfo)
    // const {fetchGitCommits} = this.props;
    // const {credential_id, origin_forth, origin_jack_name, origin_third, origin_url, name } = this.props.location.state.projectinfo
    // 统一用以下方式传projectinfo【但是　以上this.props.location.state.projectinfo　中亦可以获取到projectinfo，两者相同。以下是直接输入全路径访问，以上是页面内链接点入】：
    //　<Route exact path="/:namespace/:prjectname/commits" render={(props)=><Commits {...props} projectinfo={projectInfo}/>} />
    //　之前使用的是this.props.history.push({pathname: linkto,　state: {projectinfo: withstate},});
    const {credential_id, origin_forth, origin_jack_name, origin_third, origin_url, name } = this.props.projectinfo

    return (
      <Segment basic>
        <h2>Commits</h2>
        <CommitsForm
          // fetchGitCommits={fetchGitCommits}
          // fetchGitBranchesTags={fetchGitBranchesTags}
          credential_id={credential_id}
          origin_url={origin_url}
          origin_third={origin_third}
          origin_jack_name={origin_jack_name}
          origin_forth={origin_forth}
          name={name}
        />
        <CommitsTable />
      </Segment>
    );
  }
}

// const mapDispatchToProps = dispatch => ({
//   fetchGitCommits: (...args) => {
//     dispatch(fetchGitCommits(...args));
//   },
//   fetchGitBranchesTags: (...args) => {
//     dispatch(fetchGitBranchesTags(...args));
//   },
// });
//
// export default connect(
//   null,
//   mapDispatchToProps,
// )(Commits);
export default Commits;
