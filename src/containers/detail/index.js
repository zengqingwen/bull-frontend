import React, {Component} from 'react';
import {Segment, Table} from 'semantic-ui-react';

import './styles.css';

class Inbox extends Component {
  render() {
    return (
      <Segment basic id="content">
        <h2>Dashboard</h2>
        <Table singleLine>
          <Table.Header>
            <Table.Row>
              {/* {
                headers.map(
                  header => (
                    <Table.HeaderCell key={header}>{header}</Table.HeaderCell>))
              } */}
            </Table.Row>
          </Table.Header>
          <Table.Body>{/* {emails.map(this.renderTableRow)} */}</Table.Body>
        </Table>
      </Segment>
    );
  }
}

// const mapStateToProps = state => ({
//   emails: state.inbox.emails
// });

export default Inbox;

// const mapDispatchToProps = dispatch => ({
//   getInbox: bindActionCreators(requestInbox, dispatch),
//   getEmail: bindActionCreators(requestEmail, dispatch)
// });

// export default connect(mapStateToProps, mapDispatchToProps)(Inbox);
