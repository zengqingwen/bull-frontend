import React from 'react';
import {Button} from 'semantic-ui-react';
import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';
import PropTypes from 'prop-types';
import {toggleSidebar as toggleSidebarAction} from '../../../actions/index';

import './styles.css';

const TopNav = ({toggleSidebar}) => (
  <div id="nav-top" className="flex-container">
    <Button
      id="menu-button"
      // className="flex-item"
      icon="sidebar"
      circular
      onClick={toggleSidebar}
    />
    <div className="flex-item" style={{ width: '100%', textAlign:'center' }}>
      广告栏，留着放通知信息。
    </div>
    {/*<div className="flex-item" style={{align:'center'}}>*/}
      {/*广告栏，留着放通知信息。*/}
    {/*</div>*/}
    {/*<Message negative style={{ width: '100%', textAlign:'center' }}>*/}
      {/*<Message.Header>We're sorry we can't apply that discount.</Message.Header>*/}
      {/*/!*<p>That offer has expired</p>*!/*/}
    {/*</Message>*/}
  </div>
);

TopNav.propTypes = {
  toggleSidebar: PropTypes.func.isRequired,
};

const mapDispatchToProps = dispatch => ({
  toggleSidebar: bindActionCreators(toggleSidebarAction, dispatch),
});

export default connect(
  null,
  mapDispatchToProps,
)(TopNav);
