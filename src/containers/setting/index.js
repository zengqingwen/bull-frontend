import React, {Component} from 'react';
import {connect} from 'react-redux';
import EditProfile from '../../components/editprofile';
import {editProfileReset, editProfile} from '../../actions';
import './styles.css';

class Setting extends Component {
  render() {
    const {isEditing, error, avatar, name, editProfile, success} = this.props;

    return (
      <div className="setting">
        <EditProfile
          avatar={avatar}
          name={name}
          handleEdit={editProfile}
          isLoading={isEditing}
          error={error}
          success={success}
        />
      </div>
    );
  }
}

const mapStateToProps = state => ({
  isEditing: state.auth.isEditing,
  error: state.auth.editError,
  avatar: state.auth.avatar,
  name: state.auth.name,
  success: state.auth.editSuccess,
});

const mapDispatchToProps = dispatch => ({
  editProfile: newProfile => {
    dispatch(editProfile(newProfile));
  },
  handleClose: () => {
    dispatch(editProfileReset());
  },
});

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(Setting);
