import axios from 'axios';
import {GIT_COMMIT_URL} from '../constants';
import {getConfig} from '../../utils/config';

export const fetchGitCommitsApi = (...args) => {
  var config = getConfig()
  config.params = args[0]
  // console.log(config)
  return axios.post(GIT_COMMIT_URL, args[1], config);

  // var config = getConfig()
  // config.headers = {...config.headers, ...args[0]}
  // return axios.post(GIT_COMMIT_URL, args[1], config);
  //
};
