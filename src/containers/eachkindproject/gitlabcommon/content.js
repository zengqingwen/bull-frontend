import React, {Component} from 'react';
import {Segment, Table} from 'semantic-ui-react';

import './styles.css';

class Content extends Component {
  render() {
    return (
      <Segment basic id="content">
        <h2>Dashboard</h2>
        <Table singleLine>
          <Table.Header>
            <Table.Row>
              {/* {
                headers.map(
                  header => (
                    <Table.HeaderCell key={header}>{header}</Table.HeaderCell>))
              } */}
            </Table.Row>
          </Table.Header>
          <Table.Body>{/* {emails.map(this.renderTableRow)} */}</Table.Body>
        </Table>
      </Segment>
    );
  }
}

export default Content;
