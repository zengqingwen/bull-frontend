import {
  FETCH_HOME_REQUEST,
  FETCH_HOME_SUCCESS,
  FETCH_HOME_FAILURE,
  CHANG_EFILTER_SCOP,
  FROM_NAV_LINK,
} from '../actions/types';

const initialParamState = {
  scopInit: true,
  fromnavlink: false,
  activeItem: 'yourproject',
  scop: 'yourproject',
  keyword: '',
  sort: '',
};

const initialDataState = {
  isLoading: false,
  projects: null,
  error: null,
};

const initialState = {
  ...initialParamState,
  ...initialDataState,
};

const home = (state = initialState, action) => {
  switch (action.type) {
    case FETCH_HOME_REQUEST:
      return {
        ...state,
        isLoading: true,
        error: null,
      };
    case FETCH_HOME_SUCCESS:
      return {
        ...state,
        isLoading: false,
        projects: action.projects,
        error: null,
      };
    case FETCH_HOME_FAILURE:
      return {
        // ...initialState,
        ...state,
        error: action.error,
      };
    case CHANG_EFILTER_SCOP:
      return {
        // ...initialState,
        ...state,
        scopInit: action.item.scopInit,
        activeItem: action.item.scop,
        // ...action.item,
        scop: action.item.scop,
        keyword: action.item.keyword,
        sort: action.item.sort,
      }
    case FROM_NAV_LINK:
      return {
        // ...initialState,
        ...state,
        fromnavlink: action.item.fromnavlink,
      }
    default:
      return state;
  }
};

export default home;
