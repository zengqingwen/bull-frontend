import React, {Component} from 'react';

import {Switch, Route} from 'react-router-dom';

// import PrivateRoute from '../../../components/common/PrivateRoute';

import Sidebar from './sidebar';
import TopNav from './topnav';

import Detail from './detail';
import Content from './content';

// import Sidebar from '../../../components/sidebar';
// import TopNav from '../../../components/topnav';
// import Detail from '../../detail';

class GitlabContainer extends Component {
  render() {
    const {isToggled, projectInfo} = this.props;

    return (
      <div id="dashboard">
        <div id="main" className={isToggled ? 'toggled' : ''}>
          <TopNav />
          {/*<br /><br /><br /><div>{projectInfo.cate_type} - This is public area, but have no use.</div>*/}
          <Switch>
            <Route exact path="/:namespace/:prjectname/33" component={Content} />
            {/*<PrivateRoute exact path='' component={Detail} />*/}
            <Route exact path="/:namespace/:prjectname/" component={Detail} />
          </Switch>
        </div>
        <Sidebar
          projectInfo={projectInfo}
        />
      </div>
    )
  }
}

export default GitlabContainer;
