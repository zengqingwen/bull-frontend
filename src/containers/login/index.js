import React, {Component} from 'react';
import {
  Button,
  Form,
  Grid,
  Header,
  Image,
  Segment,
  Message,
} from 'semantic-ui-react';
import {Link, Redirect} from 'react-router-dom';
import {connect} from 'react-redux';
import {login} from '../../actions';

import LogoPNG from '../../components/logo/joy.png';
import StatusMessage from '../../components/statusmessage';

export class Login extends Component {
  constructor(props) {
    super(props);
    this.state = {
      username: '',
      password: '',
    };
  }

  handleChange = (e, {name, value}) => {
    this.setState({[name]: value});
  };

  isFormValid = () => {
    const {username, password} = this.state;

    let isFormValid = true;
    if (!username || !password) {
      isFormValid = false;
    }
    return isFormValid;
  };

  handleSubmit = e => {
    if (this.isFormValid()) {
      this.props.handleLogin(this.state.username, this.state.password);
    }
  };

  render() {
    const {isAuthenticated, isLoading, error} = this.props;
    const {username, password} = this.state;

    const statusMessage = (
      <StatusMessage
        error={error}
        errorMessage={error || 'Login Error'}
        loading={isLoading}
        loadingMessage={'Signing in'}
        type="modal"
      />
    );

    return isAuthenticated ? (
      <Redirect
        to={{
          pathname: '/dashboard',
          form: {
            from: this.props.location,
          },
        }}
      />
    ) : (
      <div className="login-form">
        {/*
        Heads up! The styles below are necessary for the correct render of this example.
        You can do same with CSS, the main idea is that all the elements up to the `Grid`
        below must have a height of 100%.
      */}
        <style>{`
        body > div,
        body > div > div,
        body > div > div > div.login-form {
          height: 100%;
        }
        .app-layout {
            margin: 0em auto;
        }
      `}</style>
        <Grid
          textAlign="center"
          style={{height: '100%'}}
          verticalAlign="middle">
          <Grid.Column style={{maxWidth: 450}}>
            <Header as="h2" color="teal" textAlign="center">
              <Image src={LogoPNG} /> Log-in to your account
            </Header>
            {statusMessage}
            <Form size="large">
              <Segment stacked>
                <Form.Input
                  required
                  fluid
                  icon="user"
                  iconPosition="left"
                  placeholder="E-mail address"
                  name="username"
                  onChange={this.handleChange}
                  value={username}
                />
                <Form.Input
                  required
                  fluid
                  icon="lock"
                  iconPosition="left"
                  placeholder="Password"
                  type="password"
                  name="password"
                  onChange={this.handleChange}
                  value={password}
                />

                <Button
                  color="teal"
                  fluid
                  size="large"
                  loading={isLoading}
                  disabled={isLoading}
                  onClick={this.handleSubmit}>
                  Login
                </Button>
              </Segment>
            </Form>
            <Message>
              New to us? <Link to="/register">Register</Link>
            </Message>
          </Grid.Column>
        </Grid>
      </div>
    );
  }
}

const mapStateToProps = state => ({
  isLoading: state.auth.isLoading,
  error: state.auth.error,
  isAuthenticated: state.auth.isAuthenticated,
});

const mapDispatchToProps = dispatch => ({
  handleLogin: (username, password) => {
    dispatch(login(username, password));
  },
});

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(Login);
