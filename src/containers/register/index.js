import React, {Component} from 'react';
import {
  Button,
  Form,
  Grid,
  Header,
  Image,
  Segment,
  Message,
} from 'semantic-ui-react';
import {Link, Redirect} from 'react-router-dom';
import {connect} from 'react-redux';
import {register} from '../../actions';

import LogoPNG from '../../components/logo/joy.png';
import StatusMessage from '../../components/statusmessage';

export class Register extends Component {
  constructor(props) {
    super(props);
    this.state = {
      username: '',
      name: '',
      email: '',
      password: '',
      checked: true,
    };
  }

  handleChange = (e, {name, value}) => {
    this.setState({[name]: value});
  };

  handleCheckbox = () => {
    this.setState({checked: !this.state.checked});
  };

  isFormValid = () => {
    const {username, name, email, password, checked} = this.state;

    let isFormValid = true;
    if (!username || !name || !email || !password || !checked) {
      isFormValid = false;
    }
    return isFormValid;
  };

  handleSubmit = e => {
    if (this.isFormValid()) {
      let data = {
        username: this.state.username,
        name: this.state.name,
        email: this.state.email,
        password: this.state.password,
      };
      this.props.handleRegister(data);
    }
  };

  render() {
    const {isAuthenticated, isLoading, error} = this.props;
    const {username, name, email, password} = this.state;

    const statusMessage = (
      <StatusMessage
        error={error}
        errorMessage={error || 'Login Error'}
        loading={isLoading}
        loadingMessage={'Registering your account'}
        type="modal"
      />
    );

    return isAuthenticated ? (
      <Redirect
        to={{
          pathname: '/dashboard',
          form: {
            from: this.props.location,
          },
        }}
      />
    ) : (
      <div className="register-form">
        {/*
        Heads up! The styles below are necessary for the correct render of this example.
        You can do same with CSS, the main idea is that all the elements up to the `Grid`
        below must have a height of 100%.
      */}
        <style>{`
        body > div,
        body > div > div,
        body > div > div > div.register-form {
          height: 100%;
        }
        .app-layout {
            margin: 0em auto;
        }
      `}</style>
        <Grid
          textAlign="center"
          style={{height: '100%'}}
          verticalAlign="middle">
          <Grid.Column style={{maxWidth: 450}}>
            <Header as="h2" color="teal" textAlign="center">
              <Image src={LogoPNG} /> Registe an account
            </Header>
            {statusMessage}
            <Form size="large">
              <Segment stacked>
                <Form.Input
                  required
                  fluid
                  icon="user"
                  iconPosition="left"
                  placeholder="Username"
                  name="username"
                  onChange={this.handleChange}
                  value={username}
                />
                <Form.Input
                  required
                  fluid
                  icon="user outline"
                  iconPosition="left"
                  placeholder="Name"
                  type="text"
                  name="name"
                  onChange={this.handleChange}
                  value={name}
                />
                <Form.Input
                  required
                  fluid
                  icon="mail"
                  iconPosition="left"
                  placeholder="Email"
                  type="email"
                  name="email"
                  onChange={this.handleChange}
                  value={email}
                />
                <Form.Input
                  required
                  fluid
                  icon="lock"
                  iconPosition="left"
                  placeholder="Password"
                  type="password"
                  name="password"
                  onChange={this.handleChange}
                  value={password}
                />
                <Form.Checkbox
                  inline
                  required
                  label="I agree to the terms and conditions"
                  name="agreement"
                  checked={this.state.checked}
                  onChange={this.handleCheckbox}
                />
                <Button
                  color="teal"
                  fluid
                  size="large"
                  loading={isLoading}
                  disabled={isLoading}
                  onClick={this.handleSubmit}>
                  Submit
                </Button>
              </Segment>
            </Form>
            <Message>
              Already have an account? <Link to="/login">Login</Link>
            </Message>
          </Grid.Column>
        </Grid>
      </div>
    );
  }
}

const mapStateToProps = state => ({
  isAuthenticated: state.auth.isAuthenticated,
  error: state.register.error,
  isLoading: state.register.isLoading,
});

const mapDispatchToProps = dispatch => ({
  handleRegister: data => {
    dispatch(register(data));
  },
});

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(Register);
