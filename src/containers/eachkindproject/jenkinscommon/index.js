import React, {Component} from 'react';

import {Switch, Route} from 'react-router-dom';

import Sidebar from './sidebar';
import TopNav from './topnav';
import Detail from '../gitlabcommon/content';

class JenkinsContainer extends Component {
  render() {
    const {isToggled, projectInfo} = this.props;

    return (
      <div id="dashboard">
        <div id="main" className={isToggled ? 'toggled' : ''}>
          <TopNav /><div>zzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzz{projectInfo.cate_type}</div>
          <Switch>
            {/* <Route exact path='33' component={Detail} /> */}
            {/* OR */}
            <Route exact path="/:namespace/:prjectname/33" component={Detail} />
          </Switch>
        </div>
        <Sidebar
          projectInfo={projectInfo}
        />
      </div>
    )
  }
}

export default JenkinsContainer;
