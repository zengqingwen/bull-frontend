import {
  FETCH_GIT_BRANCHESTAGS_REQUEST,
  FETCH_GIT_BRANCHESTAGS_SUCCESS,
  FETCH_GIT_BRANCHESTAGS_FAILURE,
} from '../types';

import {
  fetchGitBranchesTagsApi,
} from '../../api/index';
import {apiErrorHandler} from '../../utils/errorhandler';

export const fetchGitBranchesTags = (...args) => dispatch => {
  dispatch(fetchGitBranchesTagsRequest());

  fetchGitBranchesTagsApi(...args)
    .then(response => {
      dispatch(fetchGitBranchesTagsSuccess(response.data));
    })
    .catch(error => {
      const errorMessage = apiErrorHandler(error);
      dispatch(fetchGitBranchesTagsFailure(errorMessage));
    });
};

export const fetchGitBranchesTagsRequest = () => {
  return {
    type: FETCH_GIT_BRANCHESTAGS_REQUEST,
  };
};

export const fetchGitBranchesTagsSuccess = gitbranchestags => {
  return {
    type: FETCH_GIT_BRANCHESTAGS_SUCCESS,
    gitbranchestags: gitbranchestags,
  };
};

export const fetchGitBranchesTagsFailure = error => {
  return {
    type: FETCH_GIT_BRANCHESTAGS_FAILURE,
    error,
  };
};
