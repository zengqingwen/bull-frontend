import React, {Component} from 'react';
import {connect} from 'react-redux';

import {
  Form,
  Header,
  Input,
  Radio,
  Select,
  TextArea,
  Button,
  Divider,
  Label,
} from 'semantic-ui-react';

import ClearableSelect from '../../../../components/clearableselect';

import DatePicker from "react-datepicker";
import "react-datepicker/dist/react-datepicker.css";
// import DateTimePicker from 'react-datetime-picker';
// import * as Datetime from 'react-datetime';

import StatusMessage from '../../../../components/statusmessage';

import {
  fetchGitCommits,
  fetchGitBranchesTags,
} from '../../../../actions';




class CommitsForm extends Component {
  constructor(props) {
    super(props);
    this.state = {
      branchOptions: [],
      endPointOptions: [],

      branches: [],
      startPoint: "",
      endPoint: "",
      since: null,
      // until: new Date(),
      until: null,
      author:"",
      grep: "",
      files: "",
      keyword: "",
    };
  }

  // handleChange = (e, { value }) => this.setState({ currentValues: value })
  handleChange = (e, {name, value}) => {
    this.setState({[name]: value});
    console.log(this.state.branches)
  };

  handleChangeMutex = (e, {name, value}) => {
    if(name === "branches") {
      this.setState({[name]: value, startPoint: "", endPoint: ""});
    }else if(name === "startPoint" || name === "endPoint") {
      this.setState({[name]: value, branches: []});
    }
  };

  handleChangeStartDate = (date) => this.setState({ "since": date })
  handleChangeEndDate = (date) => this.setState({ "until": date })
  // 此处不能用[name] 因为这个日期插件只能传递一个date 参数
  // handleChangeDate = (date, {name}) => this.setState({ [name]: date })

  handleAddition = (e, { optiontype, value }) => {
    // console.log(optiontype)
    // console.log("#333")
    this.setState((prevState) => ({
      // branchOptions: [{ text: value, value }, ...prevState.branchOptions],
      // 此处的中括号[optiontype]专项专用，参考的是handleChange　函数中的name

      [optiontype]: [{ text: value, value }, ...prevState[[optiontype]]],
      // or as below :
      // [optiontype]: [{ text: value, value }, ...prevState[optiontype]],
    }))
  }

  getParme = () => {
    const {credential_id, origin_url, origin_third, origin_jack_name, origin_forth, name} = this.props;
    const headers = {
      "Credential-Id": credential_id,
      "Origin-Url": origin_url,
      "Origin-Third": origin_third,
      "Origin-Jack-Name": origin_jack_name,
      "Origin-Forth": origin_forth,
      "Name": name,
    }
    return headers
  }

  componentDidMount() {
    const {fetchGitBranchesTags} = this.props;
    const headers = this.getParme();
    fetchGitBranchesTags(headers);


    const {gitbranchestags} = this.props;
    const this_branches = gitbranchestags.msg.branches;
    const this_tags = gitbranchestags.msg.tags;
    const enpointBranchOptions = this_branches.map((o, index) => {
      return {key: index + "b", text: o, value: o};
    });
    const enpointTagOptions = this_tags.map((o, index) => {
      return {key: index + "t", text: o, value: o};
    });
    const endPointOptions = [
      {
        key: 'branches',
        text: (
          <span>
            <strong>Branches</strong>
          </span>
        ),
        disabled: true,
      },
      ...enpointBranchOptions,
      {
        key: 'tags',
        text: (
          <span>
            <strong>Tags</strong>
          </span>
        ),
        disabled: true,
      },
      ...enpointTagOptions,
    ];
    this.setState({endPointOptions, branchOptions: [...enpointBranchOptions]});
  }

  onSubmit = () => {
    const {fetchGitCommits} = this.props;
    const headers = this.getParme();
    // console.log(headers)
    console.log(this.state);
    // 此处使用的是解构赋值
    //　也可以使用Lodash 的 omit 方法移除不要的属性　https://segmentfault.com/q/1010000013891861?utm_source=tag-newest
    // const object = { 'a': 1, 'b': '2', 'c': 3 };
    // const result = _.omit(object, ['a', 'c']);
    // 或者用 pick 方法只留下需要的属性：
    // const object = { 'a': 1, 'b': '2', 'c': 3 };
    // const result = _.pick(object, ['a', 'c']);
    var {branchOptions, endPointOptions, keyword: G, ...formParmes} = this.state;
    // const newHeader = {...headers, ...formParmes}
    // fetchGitCommits(newHeader);
    // console.log(formParmes.since)
    // console.log(formParmes.until)
    // 以上的时间还是中国时间，当一传到form表单中的时候就变成了ＵＴＣ时间了
    var formParmes2 = {G, ...formParmes}
    fetchGitCommits(headers, formParmes2);
  };


  render() {
    const {
      branches,
      startPoint,
      endPoint,
      since,
      until,
      author,
      grep,
      files,
      keyword,
    } = this.state;




    return (
        <Form>

          {/*Dropdown 	allowAdditions can be used with multiple.*/}
          {/*https://blog.csdn.net/suwyer/article/details/81867264  react中日期时间格式化*/}
          {/*https://blog.csdn.net/qq_24147051/article/details/80678440  react日期格式化实例*/}
          <Form.Group>
            <Form.Field
              control={Select}
              label="Branches"
              search
              selection
              fluid
              clearable
              multiple
              allowAdditions
              name="branches"
              value={branches}
              optiontype="branchOptions"
              options={this.state.branchOptions}
              // options={branchOptions}
              onChange={this.handleChangeMutex}
              onAddItem={this.handleAddition}
              placeholder="Branches"
              width={4}
              size="small"
            />

            <Form.Field control={Label} label="或者" circular color={"purple"}>or</Form.Field>

            <Form.Field
              control={Select}
              label="Start point(tag or branch)"
              name="startPoint"
              search
              selection
              fluid
              clearable
              allowAdditions
              value={startPoint}
              optiontype="endPointOptions"
              options={this.state.endPointOptions}
              // options={endPointOptions}
              onChange={this.handleChangeMutex}
              onAddItem={this.handleAddition}
              placeholder="Start point(eg: branchname tagname hash HEAD^)"
              width={4}
            />
            <Form.Field
              control={Select}
              label="End point(tag or branch)"
              name="endPoint"
              search
              selection
              fluid
              clearable
              allowAdditions
              value={endPoint}
              optiontype="endPointOptions"
              options={this.state.endPointOptions}
              // options={endPointOptions}
              onChange={this.handleChangeMutex}
              onAddItem={this.handleAddition}
              placeholder="End point(eg: branchname tagname hash HEAD^)"
              width={4}
            />

          </Form.Group>


          <Form.Group>
            <div style={{padding:'6px'}}>
              <Header sub>Since</Header>
              <DatePicker
                selected={since}
                onChange={this.handleChangeStartDate}
                // onChange={this.handleChangeDate}
                dateFormat="YYYY-MM-dd"
                name="since"
              />
            </div>
            <div style={{padding:'6px'}}>
              <Header sub>Until</Header>
              <DatePicker
                selected={until}
                onChange={this.handleChangeEndDate}
                // onChange={this.handleChangeDate}
                dateFormat="YYYY-MM-dd"
                name="until"
              />
            </div>
          </Form.Group>

        <Form.Group>
          <Form.Input
            label="Author"
            name="author"
            value={author}
            onChange={this.handleChange}
            placeholder="按照提交者筛选，推荐使用提交者邮箱"
            size="mini"
            width={3}
          />
          <Form.Input
            label="Grep"
            name="grep"
            value={grep}
            onChange={this.handleChange}
            placeholder="筛选commit message中的关键字"
            size="mini"
            width={3}
          />
          <Form.Input
            label="File name"
            name="files"
            value={files}
            onChange={this.handleChange}
            placeholder="筛选文件名"
            size="mini"
            width={3}
          />
          <Form.Input
            label="Keyword"
            name="keyword"
            value={keyword}
            onChange={this.handleChange}
            placeholder="筛选修改内容关键字，支持正则(eg: \sword)"
            size="mini"
            width={3}
          />
        </Form.Group>

        <Form.Group inline>
          <label>merges OR no-merges</label>
          <Form.Radio
            label='merges'
            value='sm'
            checked={'sm' === 'sm'}
            onChange={this.handleChange}
          />
          <Form.Radio
            label='no-merges'
            value='md'
            checked={'sm' === 'md'}
            onChange={this.handleChange}
          />
        </Form.Group>

        <Divider />

        <Form.Group>
          <Form.Checkbox checked label='Short commit id' width={3} />
          <Form.Checkbox checked label='title' width={3} />
          <Form.Checkbox label='datetime' width={3} />
          <Form.Checkbox label='author' width={3} />
        </Form.Group>

        <Divider />

        <Form.Checkbox label='统计提交数量(--stat)' />



          {/*ｂｅｌｏｗｓ　ａｒｅ　ｂｕｔｔｏｎｓ*/}
          <Form.Field
            control={Button}
            color="green"
            onClick={this.onSubmit}
            // loading={newProjectLoading}
            // disabled={newProjectLoading}
          >
            Get commits
          </Form.Field>
        </Form>

    )
  }
}

const mapStateToProps = state => ({
  isLoading: state.gitBranchesTags.isLoading,
  gitbranchestags: state.gitBranchesTags.gitbranchestags,
  error: state.gitBranchesTags.error,
});

const mapDispatchToProps = dispatch => ({
  fetchGitCommits: (...args) => {
    dispatch(fetchGitCommits(...args));
  },
  fetchGitBranchesTags: (...args) => {
    dispatch(fetchGitBranchesTags(...args));
  },
});

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(CommitsForm);
