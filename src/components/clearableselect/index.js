import React, { Component } from "react";
import { Select } from "semantic-ui-react";

const styles = {
  selectWrapper: {
    position: "relative"
  },
  select: {
    paddingRight: "4em"
  },
  clearIcon: {
    position: "absolute",
    top: "1px",
    right: "1.8em",
    lineHeight: "1em",
    padding: "0.46em 0",
    fontSize: "1.5em",
    width: "auto",
    zIndex: 3,
    cursor: "pointer"
  }
};

export default class ClearableSelect extends Component {
  render() {
    const { name, onChange, value, ...otherProps } = this.props;
    const showClear = value != null;

    return (
      <div style={styles.selectWrapper}>
        <Select
          name={name}
          onChange={onChange}
          value={value}
          {...otherProps}
          style={styles.select}
        />
        {showClear && (
          <i
            onClick={() => onChange({}, { name, value: undefined })}
            className="icon"
            style={styles.clearIcon}
          >
            &times;
          </i>
        )}
      </div>
    );
  }
}
