import React, {Component} from 'react';
import {connect} from 'react-redux';
import {fetchCategory, fetchCredential, createProject, initialCreateProjectPage} from '../../actions';

import NewProjectPage from '../../components/newprojectpage';

class CreateProjectContainer extends Component {
  componentDidMount() {
    this.props.fetchCategory();
    this.props.fetchCredential();
    this.props.initialCreateProjectPage()
  }

  render() {
    const {
      isLoading,
      categories,
      credentials,
      error,
      username,
      createProject,
      newProjectLoading,
      newProjectSuccess,
      newProjectError,
      newProject,
    } = this.props;
    return (
      <NewProjectPage
        error={error}
        isLoading={isLoading}
        categories={categories}
        credentials={credentials}
        username={username}
        createProject={createProject}
        newProjectLoading={newProjectLoading}
        newProjectSuccess={newProjectSuccess}
        newProjectError={newProjectError}
        newProject={newProject}
      />
    );
  }
}

const mapStateToProps = state => ({
  isLoading: state.credential.isLoading,
  categories: state.category.categories,
  credentials: state.credential.credentials,
  error: state.credential.error,
  username: state.auth.username,

  newProjectLoading: state.project.newProjectLoading,
  newProjectSuccess: state.project.newProjectSuccess,
  newProjectError: state.project.newProjectError,
  newProject: state.project.newProject,
});

const mapDispatchToProps = dispatch => ({
  fetchCategory: () => {
    dispatch(fetchCategory());
  },
  fetchCredential: category => {
    dispatch(fetchCredential(category));
  },
  createProject: newProject => {
    dispatch(createProject(newProject));
  },
  initialCreateProjectPage: () => {
    dispatch(initialCreateProjectPage());
  },
});

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(CreateProjectContainer);
