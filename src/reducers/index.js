import {combineReducers} from 'redux';

import sidebarToggled from './sidebar';
import auth from './auth';
import register from './register';
import users from './users';
import userProfile from './userprofile';
import category from './category';
import credential from './credential';
import home from './home';
import project from './project';

import gitCommits from './remotes/gitcommit';
import gitBranchesTags from './remotes/gitbranchtag';

const rootReducer = combineReducers({
  isSidebarToggled: sidebarToggled,
  auth,
  register,
  users,
  userProfile,
  category,
  credential,
  home,
  project,
  gitCommits,
  gitBranchesTags,
});

export default rootReducer;
