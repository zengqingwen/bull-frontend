import React, {Component} from 'react';
import {connect} from 'react-redux';
import {Dropdown, Menu, Icon} from 'semantic-ui-react';

import {withRouter} from 'react-router';

import Logo from '../logo';

import {changeFilterScop, fromNavLink} from '../../actions';

class Navlink extends Component {
  state = {activeItem: 'dashboard'};

  handleItemClick = (e, {name}) => {
    this.setState({activeItem: name});
    this.props.history.push(`/${name}`);
  };

  handleDropdownItemClick = (e, {scop}) => {
    this.props.history.push({
      pathname: '/dashboard',
    });
    var query_args = {
      scop,
      keyword: '',
      sort: '',
    }
    // console.log(query_args)
    this.props.changeFilterScop({...query_args, scopInit: false});
    this.props.fromNavLink({fromnavlink: true});
  }

  render() {
    const {activeItem} = this.state || {};

    return (
      <>
        <Menu.Item header>
          <Logo />
        </Menu.Item>

        <Menu.Item
          as="a"
          name="dashboard"
          active={activeItem === 'dashboard'}
          onClick={this.handleItemClick}>
          <Icon name="home" />
          Home
        </Menu.Item>

        <Menu.Item
          name="users"
          active={activeItem === 'users'}
          onClick={this.handleItemClick}>
          <Icon name="users" />
          Users
        </Menu.Item>

        <Menu.Item name="github" href="https://github.com" target="_blank">
          <Icon name="github" />
          Github
        </Menu.Item>

        <Dropdown item simple text="Projects">
          <Dropdown.Menu>
            <Dropdown.Item scop='yourproject' onClick={this.handleDropdownItemClick}>Your projects</Dropdown.Item>
            <Dropdown.Item scop='starproject' onClick={this.handleDropdownItemClick}>Starred projects</Dropdown.Item>
            <Dropdown.Divider />
            <Dropdown.Header>OR</Dropdown.Header>
            <Dropdown.Item>
              <i className="dropdown icon" />
              <span className="text">Frequently visited</span>
              <Dropdown.Menu>
                <Dropdown.Item>List Item</Dropdown.Item>
                <Dropdown.Item>List Item</Dropdown.Item>
              </Dropdown.Menu>
            </Dropdown.Item>
            <Dropdown.Item>List Item</Dropdown.Item>
          </Dropdown.Menu>
        </Dropdown>
      </>
    );
  }
}

const mapDispatchToProps = dispatch => ({
  changeFilterScop: (item) => {
    dispatch(changeFilterScop(item));
  },
  fromNavLink: (item) => {
    dispatch(fromNavLink(item));
  },
});

export default connect(null, mapDispatchToProps)(withRouter(Navlink));
