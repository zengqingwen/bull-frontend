import React from 'react';
import logo from './joy.png';
import './styles.css';

const Logo = () => {
  return (
    <div className="logoContainer">
      <img src={logo} className="logo" alt="logo" />
      <div className="logoTitle">BullSocket</div>
    </div>
  );
};

export default Logo;
