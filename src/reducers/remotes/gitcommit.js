import {
  INITIAL_GIT_COMMITS,
  FETCH_GIT_COMMITS_REQUEST,
  FETCH_GIT_COMMITS_SUCCESS,
  FETCH_GIT_COMMITS_FAILURE,
} from '../../actions/types';

const initialState = {
  isLoading: false,
  gitcommits: null,
  error: null,
};

const gitCommits = (state = initialState, action) => {
  switch (action.type) {
    case INITIAL_GIT_COMMITS:
      return {
        ...state,
        isLoading: false,
        gitcommits: null,
        error: null,
      };
    case FETCH_GIT_COMMITS_REQUEST:
      return {
        ...state,
        isLoading: true,
        error: null,
      };
    case FETCH_GIT_COMMITS_SUCCESS:
      return {
        ...state,
        isLoading: false,
        gitcommits: action.gitcommits,
        error: null,
      };
    case FETCH_GIT_COMMITS_FAILURE:
      return {
        ...state,
        isLoading: false,
        error: action.error,
      };
    default:
      return state;
  }
};

export default gitCommits;