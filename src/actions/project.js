import {
  INITIAL_CREATE_PROJECTPAGE,
  CREATE_PROJECT_REQUEST,
  CREATE_PROJECT_SUCCESS,
  CREATE_PROJECT_FAILURE,

  INITIAL_FETCH_PROJECT_BY_NAME,
  FETCH_PROJECT_BY_NAME_REQUEST,
  FETCH_PROJECT_BY_NAME_SUCCESS,
  FETCH_PROJECT_BY_NAME_FAILURE,
} from './types';
import {createProjectApi, fetchProjectByNameApi} from '../api';
import {apiErrorHandler} from '../utils/errorhandler';



export const initialCreateProjectPage = () => {
  return {
    type: INITIAL_CREATE_PROJECTPAGE,
  }
}

export const createProject = newProject => dispatch => {
  dispatch(createProjectRequest(newProject));

  createProjectApi(newProject)
    .then(response => {
      dispatch(createProjectSuccess(response.data));
    })
    .catch(error => {
      const errorMessage = apiErrorHandler(error);
      dispatch(createProjectFailure(errorMessage));
    });
};

export const createProjectRequest = newProject => {
  return {
    type: CREATE_PROJECT_REQUEST,
    newProject,
  };
};

export const createProjectSuccess = newProject => {
  return {
    type: CREATE_PROJECT_SUCCESS,
    newProject,
  };
};

export const createProjectFailure = error => {
  return {
    type: CREATE_PROJECT_FAILURE,
    error,
  };
};


export const initialFetchProjectByName = () => {
  return {
    type: INITIAL_FETCH_PROJECT_BY_NAME,
  };
};

export const fetchProjectByName = (namespace, prjectname) => dispatch => {
  dispatch(fetchProjectByNameRequest());

  fetchProjectByNameApi(namespace, prjectname)
    .then(response => {
      dispatch(fetchProjectByNameSuccess(response.data));
    })
    .catch(error => {
      const errorMessage = apiErrorHandler(error);
      dispatch(fetchProjectByNameFailure(errorMessage));
    });
};

export const fetchProjectByNameRequest = () => {
  return {
    type: FETCH_PROJECT_BY_NAME_REQUEST,
  };
};

export const fetchProjectByNameSuccess = data => {
  return {
    type: FETCH_PROJECT_BY_NAME_SUCCESS,
    project: data,
  };
};

export const fetchProjectByNameFailure = error => {
  return {
    type: FETCH_PROJECT_BY_NAME_FAILURE,
    error,
  };
};
