import React, {Component} from 'react';
import {connect} from 'react-redux';

import StatusMessage from '../../../../components/statusmessage';
import {Table} from 'semantic-ui-react';

import {
  initialGitCommits,
} from '../../../../actions';


class CommitsTable extends Component {
  componentDidMount() {
    this.props.initialGitCommits();
  }

  renderTableRow = (content, index, stat_dispaly, headers_display) => {
    // console.log("ccccccccc")
    // console.log(content)

    if(stat_dispaly) {
      const con_stat = content["stats"]
      var con_stat_chan, con_stat_ins, con_stat_del
      if(con_stat) {
        con_stat_chan = con_stat["changecnt"] || 0
        con_stat_chan += "个文件被修改"
        con_stat_ins = con_stat["insertcnt"] || 0
        con_stat_del = con_stat["deletecnt"] || 0
      }else {
        con_stat_chan = con_stat_ins = con_stat_del = "merge point"
      }

      const con_effect = content["effectfiles"]
      var con_effect_files = ""
      if(con_effect) {
        Object.keys(con_effect).map((key) => {
          con_effect_files += `${key} => ${con_effect[[key]]}&#10;&#13;`
          return true;
        })
      }else {
        con_effect_files = "merge point"
      }
    }



    return (
      <Table.Row
        key={content.shortcommit}
        // onClick={() => this.props.getEmail({ id: content.shortcommit })}
      >
        {/*<Table.Cell title="bbb\n&#10;cc&#13;&#13;&#13;&#13;&#13;c\rddd">{content["mid"]}</Table.Cell>*/}
        {/*<Table.Cell>{content.subject}</Table.Cell>*/}
        {/*<Table.Cell>{`${content.text.substring(0, 100)}...`}</Table.Cell>*/}
        {/*<Table.Cell>{content.date}</Table.Cell>*/}

        {
          Object.keys(headers_display).map((key) => (
            <Table.Cell key={key}>{content[headers_display[[key]]]}</Table.Cell>
          ))
        }

        {
          stat_dispaly
            ? <>
                <Table.Cell title={con_effect_files}>{con_stat_chan}</Table.Cell>
                {/*<Table.Cell title="bbb\n&#10;cc&#13;&#13;&#13;&#13;&#13;c\rddd">{con_stat_ins}</Table.Cell>*/}
                <Table.Cell>{con_stat_ins}</Table.Cell>
                <Table.Cell>{con_stat_del}</Table.Cell>
                {/*<Table.Cell>{content["stats"]["changecnt"] || 0}</Table.Cell>*/}
                {/*<Table.Cell>{content["stats"]["insertcnt"] || 0}</Table.Cell>*/}
                {/*<Table.Cell>{content["stats"]["deletecnt"] || 0}</Table.Cell>*/}
              </>
            : null
        }
      </Table.Row>
    )
  };

  render() {
    var {isLoading, error, gitcommits} = this.props;

    if (error || !gitcommits || isLoading || gitcommits.length === 0 ) {
      return (
        <StatusMessage
          // error={error || !gitcommits}
          error={error}
          // errorClassName="users-error"
          errorMessage={error}
          loading={isLoading}
          loadingMessage={`We are fetching commits for you`}
          // nothing={gitcommits && gitcommits.length === 0}
          nothing={gitcommits === null}
          nothingMessage={`No commits to display`}
          // nothingClassName="users-error"
          type="default"
        />
      );
    }

    // if(!gitcommits) {
    //   gitcommits = {data:{headers:[], contents:[]}}
    // }
    // gitcommits = {data:{headers:[], contents:[]}}
    console.log("bbbbbbbbbbbbbbbb")
    console.log("dddd")
    console.log(gitcommits)
    const {data: {headers, contents}} = gitcommits;
    const [stat, ...rest_headers] = headers;

    var stat_dispaly = false;
    var headers_display = headers;

    if(stat === "stat") {
      stat_dispaly = true;
      headers_display = rest_headers
    }


    return (
      <Table singleLine>

        <Table.Header>
          <Table.Row>
            {
              headers_display.map(
                header => (
                  stat_dispaly
                    ? <Table.HeaderCell key={header} rowSpan='2'>{header}</Table.HeaderCell>
                    : <Table.HeaderCell key={header}>{header}</Table.HeaderCell>
                )
              )
            }
            {
              stat_dispaly
                ? <Table.HeaderCell key={stat} colSpan='3'>{stat}</Table.HeaderCell>
                : null
            }
          </Table.Row>
          {/*<Table.Row>*/}
            {stat_dispaly
              ? <Table.Row>
                  <Table.HeaderCell>Affect file cnt</Table.HeaderCell>
                  <Table.HeaderCell>Adition</Table.HeaderCell>
                  <Table.HeaderCell>Deletion</Table.HeaderCell>
                </Table.Row>
              : null
            }
          {/*</Table.Row>*/}
        </Table.Header>
        <Table.Body>
          {contents.map((content, index) => this.renderTableRow(content, index, stat_dispaly, headers_display))}
        </Table.Body>

      </Table>
    )
  }
}

const mapStateToProps = state => ({
  isLoading: state.gitCommits.isLoading,
  gitcommits: state.gitCommits.gitcommits,
  error: state.gitCommits.error,
});

const mapDispatchToProps = dispatch => ({
  initialGitCommits: () => {
    dispatch(initialGitCommits());
  },
});

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(CommitsTable);
