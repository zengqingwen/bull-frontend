import React, {Component} from 'react';
import {withRouter} from 'react-router';
import {connect} from 'react-redux';

import {Menu} from 'semantic-ui-react';
import PropTypes from 'prop-types';

import './styles.css';

class Sidebar extends Component {
  state = {activeItem: 'details'};

  static propTypes = {
    isToggled: PropTypes.bool.isRequired,
  };

  // handleItemClick = (e, {name}) => this.setState({activeItem: name});
  handleItemClick = (e, {name, linkto, withstate}) => {
    this.setState({activeItem: name});
    // console.log("$$$$$$$$$")
    // console.log(name)
    // console.log(linkto)
    // console.log(withstate)
    this.props.history.push({
      // 虽然src/containers/eachkindproject/gitlabgit/index.js 中使用了render={(props)=><Commits {...props} projectinfo={projectInfo}/>}　这样传projectinfo
      //　但是此处是Sidebar 模块，所以还是需要靠以下来传递projectinfo，以上是另一个模块，【两者各用各的projectInfo，虽然值都是一样的】
      // 此处是为了方便/home/zengqingwen/桌面/bull/src/containers/project/index.js文件可以有projectInfo　来做判断
      pathname: linkto,　state: {projectinfo: withstate},
      // pathname: linkto,　
    });
  }

  render() {
    const {isToggled, projectInfo} = this.props;
    const {activeItem} = this.state || {};

    return (
      <Menu
        id="sidebar"
        pointing
        secondary
        vertical
        className={isToggled ? 'toggled' : ''}>
        <Menu.Header>{projectInfo.name}</Menu.Header>
        <Menu.Item>
          <Menu.Header>Overview</Menu.Header>

          <Menu.Menu>

            {/*每个Menu.Item必须要有这几项　name、active、linkto、withstate、onClick*/}
            <Menu.Item
              name="details"
              active={activeItem === 'details'}
              linkto={`/${projectInfo.owner}/${projectInfo.name}`}
              withstate={projectInfo}
              onClick={this.handleItemClick}
            />
            {/*<Menu.Item>*/}
              {/*<Link to={`/${projectInfo.owner}/${projectInfo.name}`}>enterprise</Link>*/}
            {/*</Menu.Item>*/}

            <Menu.Item
              name="activity"
              active={activeItem === 'activity'}
              linkto={`/${projectInfo.owner}/${projectInfo.name}/activity`}
              withstate={projectInfo}
              onClick={this.handleItemClick}
            />
            {/*<Menu.Item>*/}
              {/*<Link to={`/${projectInfo.owner}/${projectInfo.name}/33`}>consumer</Link>*/}
            {/*</Menu.Item>*/}

          </Menu.Menu>
        </Menu.Item>

        <Menu.Item>
          <Menu.Header>Repository</Menu.Header>

          <Menu.Menu>
            <Menu.Item
              name="commits"
              active={activeItem === 'commits'}
              // linkto='' 如果路径不以/ 开头，则是相对上次的相对路径，一般不要使用
              linkto={`/${projectInfo.owner}/${projectInfo.name}/commits`}
              // withstate={projectInfo}　一定要带上projectInfo信息，要不然跑错project的页面展示了（既可能回不到commongitlab页面了）
              withstate={projectInfo}
              onClick={this.handleItemClick}
            />
            <Menu.Item
              name="python"
              active={activeItem === 'python'}
              onClick={this.handleItemClick}
            />
            <Menu.Item
              name="php"
              active={activeItem === 'php'}
              onClick={this.handleItemClick}
            />
          </Menu.Menu>
        </Menu.Item>

        <Menu.Item>
          <Menu.Header>Hosting</Menu.Header>

          <Menu.Menu>
            <Menu.Item
              name="shared"
              active={activeItem === 'shared'}
              onClick={this.handleItemClick}
            />
            <Menu.Item
              name="dedicated"
              active={activeItem === 'dedicated'}
              onClick={this.handleItemClick}
            />
          </Menu.Menu>
        </Menu.Item>

        <Menu.Item>
          <Menu.Header>Support</Menu.Header>

          <Menu.Menu>
            <Menu.Item
              name="email"
              active={activeItem === 'email'}
              onClick={this.handleItemClick}>
              E-mail Support
            </Menu.Item>

            <Menu.Item
              name="faq"
              active={activeItem === 'faq'}
              onClick={this.handleItemClick}>
              FAQs
            </Menu.Item>
          </Menu.Menu>
        </Menu.Item>
      </Menu>
    );
  }
}

const mapStateToProps = state => ({
  isToggled: state.isSidebarToggled,
});

export default connect(mapStateToProps)(withRouter(Sidebar));
