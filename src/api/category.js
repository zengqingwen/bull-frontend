import axios from 'axios';
import {CATEGORY_URL} from './constants';
import {getConfig} from '../utils/config';

export const fetchCategoryApi = () => {
  return axios.get(CATEGORY_URL, getConfig());
};
