import React, {Component} from 'react';
import {Container, Menu} from 'semantic-ui-react';

import Navlink from '../../components/navlink';
import UserMenu from '../../components/usermenu';

import {logout} from '../../actions';

import {connect} from 'react-redux';

import './styles.css';

class HeaderContainer extends Component {
  render() {
    const {
      isAuthenticated,
      username,
      name,
      avatar,
      handleLogout,
      isLoading,
    } = this.props;

    if (!isAuthenticated) {
      return <></>;
    }

    return (
      <Menu fixed="top" inverted className="headerContainer">
        <Container>
          <Navlink />

          <UserMenu
            logout={handleLogout}
            username={username}
            name={name}
            avatar={avatar}
            isLoading={isLoading}
          />
        </Container>
      </Menu>
    );
  }
}

const mapStateToProps = state => ({
  username: state.auth.username,
  name: state.auth.name,
  avatar: state.auth.avatar,
  isAuthenticated: state.auth.isAuthenticated,
  isLoading: state.auth.isLoading,
});

const mapDispatchToProps = dispatch => ({
  handleLogout: () => {
    dispatch(logout());
  },
});

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(HeaderContainer);
