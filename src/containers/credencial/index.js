import React, {Component} from 'react';
import {connect} from 'react-redux';

import {
  fetchCategory,
  createCredentialSave,
  createCredentialToggle,
  createCredential,
  deleteCredential,
  // editCredential,
  fetchCredential,
} from '../../actions';

import CredentialList from '../../components/credentiallist';
import NewCredential from '../../components/newcredential';

class CredencialContainer extends Component {
  componentDidMount() {
    this.props.fetchCategory();
    this.props.fetchCredential();
  }

  render() {
    const {
      isLoading,
      categories,
      credentials,
      errorCategory,
      error,
      newCredentialLoading,
      newCredentialSuccess,
      newCredentialId,
      newCredentialName,
      newCredentialUsername,
      newCredentialToken,
      newCredentialCategory,
      newCredentialError,
      newCredentialShow,
      createCredential,
      createCredentialSave,
      createCredentialToggle,
      deleteCredential,
    } = this.props;
    return (
      <div>
        <NewCredential
          isLoading={newCredentialLoading}
          success={newCredentialSuccess}
          categories={categories}
          id={newCredentialId}
          name={newCredentialName}
          username={newCredentialUsername}
          token={newCredentialToken}
          category={newCredentialCategory}
          errorCategory={errorCategory}
          error={newCredentialError}
          showEditor={newCredentialShow}
          createCredential={createCredential}
          updateNewCredential={createCredentialSave}
          toggleShowEditor={createCredentialToggle}
          maxLength={2000}
        />
        <CredentialList
          isLoading={isLoading}
          credentials={credentials}
          deleteCredential={deleteCredential}
          error={error}
        />
      </div>
    );
  }
}

const mapStateToProps = state => ({
  isLoading: state.credential.isLoading,
  categories: state.category.categories,
  credentials: state.credential.credentials,
  errorCategory: state.category.error,
  error: state.credential.error,
  newCredentialLoading: state.credential.newCredentialLoading,
  newCredentialSuccess: state.credential.newCredentialSuccess,
  newCredentialId: state.credential.newCredentialId,
  newCredentialName: state.credential.newCredentialName,
  newCredentialUsername: state.credential.newCredentialUsername,
  newCredentialToken: state.credential.newCredentialToken,
  newCredentialCategory: state.credential.newCredentialCategory,
  newCredentialError: state.credential.newCredentialError,
  newCredentialShow: state.credential.newCredentialShow,
});

const mapDispatchToProps = dispatch => ({
  fetchCategory: () => {
    dispatch(fetchCategory());
  },
  fetchCredential: () => {
    dispatch(fetchCredential());
  },
  deleteCredential: id => {
    dispatch(deleteCredential(id));
  },
  createCredential: newCredential => {
    dispatch(createCredential(newCredential));
  },
  createCredentialSave: newCredential => {
    dispatch(createCredentialSave(newCredential));
  },
  createCredentialToggle: () => {
    dispatch(createCredentialToggle());
  },
});

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(CredencialContainer);
