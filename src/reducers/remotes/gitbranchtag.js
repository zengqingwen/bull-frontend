import {
  FETCH_GIT_BRANCHESTAGS_REQUEST,
  FETCH_GIT_BRANCHESTAGS_SUCCESS,
  FETCH_GIT_BRANCHESTAGS_FAILURE,
} from '../../actions/types';

const initialState = {
  isLoading: false,
  gitbranchestags: null,
  error: null,
};

const gitBranchesTags = (state = initialState, action) => {
  switch (action.type) {
    case FETCH_GIT_BRANCHESTAGS_REQUEST:
      return {
        ...state,
        isLoading: true,
        error: null,
      };
    case FETCH_GIT_BRANCHESTAGS_SUCCESS:
      return {
        ...state,
        isLoading: false,
        gitbranchestags: action.gitbranchestags,
        error: null,
      };
    case FETCH_GIT_BRANCHESTAGS_FAILURE:
      return {
        ...state,
        isLoading: false,
        error: action.error,
      };
    default:
      return state;
  }
};

export default gitBranchesTags;