import React, {Fragment} from 'react';

import './App.css';

import {Provider} from 'react-redux';

import {BrowserRouter, Switch, Route, Redirect} from 'react-router-dom';
import {PersistGate} from 'redux-persist/integration/react';
import Loader from './components/loader';

import store, {persistor} from './store';

import HeaderContainer from './containers/header';
import HomeContainer from './containers/home';
import LabelContainer from './containers/label';
import ProjectContainer from './containers/project';
import LoginContainer from './containers/login';
import RegisterContainer from './containers/register';
import UsersContainer from './containers/users';
import UserProfileContainer from './containers/userprofile';
import SettingContainer from './containers/setting';
import CredencialContainer from './containers/credencial';
import CreateProjectContainer from './containers/createproject';

import NotFoundPage from './components/notfoundpage';
import PrivateRoute from './components/common/PrivateRoute';

export default () => (
  <Provider store={store}>
    <PersistGate loading={<Loader />} persistor={persistor}>
      <BrowserRouter>
        <Fragment>
          <div className="app-layout">
            <HeaderContainer />
            <Switch>
              <PrivateRoute path="/dashboard" exact component={HomeContainer} />
              <PrivateRoute path="/users" component={UsersContainer} />
              <PrivateRoute
                path="/user/:username"
                component={UserProfileContainer}
              />
              <PrivateRoute
                path="/setting"
                exact
                component={SettingContainer}
              />
              <PrivateRoute
                path="/dashboard/credencial"
                exact
                component={CredencialContainer}
              />
              <PrivateRoute
                path="/projects/new"
                exact
                component={CreateProjectContainer}
              />
              <Route
                path="/dashboard/labels"
                exact
                component={LabelContainer}
              />
              <PrivateRoute
                path="/:namespace/:prjectname"
                component={ProjectContainer}
              />
              {/* <Route path="/:namespace" component={LabelContainer} /> */}
              <Route path="/login" component={LoginContainer} />
              <Route path="/register" component={RegisterContainer} />
              <Route
                path="/"
                exact
                render={() => <Redirect to="/dashboard" />}
              />
              <PrivateRoute component={NotFoundPage} />
            </Switch>
          </div>
        </Fragment>
      </BrowserRouter>
    </PersistGate>
  </Provider>
);
