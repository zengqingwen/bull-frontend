import React, {Component} from 'react';
import { Link } from 'react-router-dom'

import {Button, Grid, List, Label} from 'semantic-ui-react';
import StatusMessage from '../statusmessage';

export default class ProjectList extends Component {
  render() {
    const {isLoading, projects, error} = this.props;

    if (error || !projects || isLoading || projects.length === 0) {
      return (
        <StatusMessage
          error={error || !projects}
          errorClassName="home-error"
          errorMessage={error}
          loading={isLoading}
          loadingMessage={'We are fetching the homepage for you'}
          nothing={projects && projects.length === 0}
          nothingMessage={'No project to display'}
          nothingClassName="home-error"
          type="default"
        />
      );
    }

    const projectCardList = projects.map(project => {
      const {
        id,
        name,
        // origin_url,
        // origin_jack_name,
        // blocked,
        // visible,
        // creator,
        // created_at,
        cate_type,
        cate_icon_color,
        members_count,
        create_time_natural,
        owner,
      } = project;
      let cate_type2 = cate_type.split('-')[0];

      if (['gitlab', 'github', 'jenkins'].indexOf(cate_type2) < 0) {
        cate_type2 = 'creative commons';
      }

      return (
        <List.Item key={id}>
          <List.Content floated="right">
            <Button as="div" labelPosition="right">
              <Button circular icon="users" />
              <Label as="a" circular basic pointing="left">
                {members_count}
              </Label>
            </Button>
          </List.Content>
          <List.Icon
            name={cate_type2}
            color={cate_icon_color}
            size="large"
            verticalAlign="middle"
          />
          <List.Content>
            <List.Header>
              <Link to={{
                pathname:`/${owner}/${name}`,
                // hash:'#ahash',
                // query:{foo: 'foo', boo:'boo'},
                state:{projectinfo:project}
              }}
              >
                {owner} / {name}
              </Link>
              {/*{owner} / {name}*/}
            </List.Header>
            <List.Description as="a">
              {owner} created {create_time_natural}
            </List.Description>
          </List.Content>
        </List.Item>
      );
    });

    return (
      <Grid columns={1} doubling>
        <Grid.Column>
          <List animated divided relaxed verticalAlign="middle" celled>
            {projectCardList}
          </List>
        </Grid.Column>
      </Grid>
    );
  }
}
