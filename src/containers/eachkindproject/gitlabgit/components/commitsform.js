import React, {Component, Fragment} from 'react';
import {connect} from 'react-redux';

import {
  Form,
  Button,
  // Divider,
  Icon,
} from 'semantic-ui-react';


// import DatePicker from "react-datepicker";
import "react-datepicker/dist/react-datepicker.css";
// import DateTimePicker from 'react-datetime-picker';
// import * as Datetime from 'react-datetime';

// import ClearableSelect from '../../../../components/clearableselect';
import StatusMessage from '../../../../components/statusmessage';

// ①
import {data} from './commitsformdata';
import {
  addfield,
  getFormatedData,
  // changeValueMulti,
  // handleAdditionMulti,
} from '../../../../utils/formcomponents'

import {
  fetchGitCommits,
  fetchGitBranchesTags,
} from '../../../../actions';


class CommitsForm extends Component {
  constructor(props) {
    super(props);
    this.state = {
      showmore: false,
      response: "",
    };
  }

  // handleChange = (e, { value }) => this.setState({ currentValues: value })
  handleChange = (e, {name, value}) => {
    this.setState({[name]: value});
    console.log(this.state.branches)
  };


  inputInnerDivider = (key, label) => {
    return {
      key: key,
      text: (
        <span>
          <strong>{label}</strong>
        </span>
      ),
      disabled: true,
    }
  }

  getParme = () => {
    const {credential_id, origin_url, origin_third, origin_jack_name, origin_forth, name} = this.props;
    const headers = {
      "Credential-Id": credential_id,
      "Origin-Url": origin_url,
      "Origin-Third": origin_third,
      "Origin-Jack-Name": origin_jack_name,
      "Origin-Forth": origin_forth,
      "Name": name,
    }
    return headers
  }

  componentDidMount() {
    const {fetchGitBranchesTags} = this.props;
    const headers = this.getParme();
    fetchGitBranchesTags(headers);


    if(!this.props.gitbranchestags){
      return
    }

    const {
      gitbranchestags: {
        data: {
          branches: this_branches,
          tags: this_tags,
          default_branch: this_default_branch,
        }
      }
    } = this.props;
    const enpointBranchOptions = this_branches.map((o, index) => {
      return {key: index + "b", text: o, value: o};
    });
    const enpointTagOptions = this_tags.map((o, index) => {
      return {key: index + "t", text: o, value: o};
    });
    const endPointOptions = [
      this.inputInnerDivider("branches", "Branches"),
      ...enpointBranchOptions,
      this.inputInnerDivider("tags", "Tags"),
      ...enpointTagOptions,
    ];

    // mff
    var fetchData = getFormatedData(data);
    fetchData.CUSTOMER1[0].options = enpointBranchOptions;
    fetchData.CUSTOMER1[0].value = [this_default_branch];
    fetchData.CUSTOMER1[2].options = endPointOptions;
    fetchData.CUSTOMER1[3].options = endPointOptions;
    this.setState(
      prevState => ({
        ...prevState,
        response: fetchData
      }),
      () => {
        console.log(this.state);
      }
    );

    console.log("############")
    console.log(this.state) // this.state.response 还没有值，还是""
  }




  onSubmit = () => {
    const {fetchGitCommits} = this.props;
    const headers = this.getParme();

    const { response } = this.state;
    console.log("333333333333")
    console.log(response)
    var formParmes2 = {};
    Object.keys(response).map((key, index) => {
      response[key].map(({ name, dataType, value, checked }, i) => {
        // ②　区分ｌａｂｅｌ　和ｃｈｅｃｋｂｏｘ
        if(dataType === "SPAN") {
          return null; // return false <=> break
        }else if(dataType === "SINGLECHECKBOX") {
          formParmes2[[name]] = checked;
        }else {
          formParmes2[[name]] = value;
        }
        return null;　
      })
      return null;
    })
    const formParmes = {...formParmes2}
    console.log("4444444444444")
    console.log(formParmes)
    fetchGitCommits(headers, formParmes);
  };


  foldOrNot = (foldornot) => {
    const { response } = this.state;
    Object.keys(response).map((key, index) => {
      response[key].map(({ name, dataType, value, checked }, i) => {
        // ②　区分ｌａｂｅｌ　和ｃｈｅｃｋｂｏｘ
        if(dataType === "SPAN") {
          return null; // return false <=> break
        }
        if(name !== "branches" && name !== "startPoint" && name !== "endPoint"){
          if(foldornot === "unfold") {
            response[key][i].styleclass = ""
            this.setState({
              response,
              showmore: true,
            });
          }else {
            if(foldornot === "foldunsave") {
              const initial_states = {
                since: null,
                until: null,

                author: [],
                grep: [],
                files: [],
                G: "",

                merges_or_no: "default",

                col_shortcommit: true,
                col_title: true,
                col_datetime: true,
                col_author: true,

                stat: false,
              };
              if(dataType === "SINGLECHECKBOX") {
                response[key][i].checked = initial_states[[name]]
              }else {
                response[key][i].value = initial_states[[name]]
              }
            }
            response[key][i].styleclass = "hidden"
            this.setState({
              response,
              showmore: false,
            });
          }
        }
        return null;
      })
      return null;
    })
  }

  showMore = (showmore) => {
    if(!showmore) {
      return (
        <Form.Field
          control={Button}
          color="blue"
          onClick={() => this.foldOrNot("unfold")}
        >
          <Icon name="folder open" />
          Show more items
        </Form.Field>
      )
    }else {
      return (
        <>
          <Form.Field
            control={Button}
            color="red"
            onClick={() => this.foldOrNot("foldsave")}
          >
            <Icon name="save" />
            Save Draft
          </Form.Field>
          <Form.Field
            control={Button}
            // color="green"
            onClick={() => this.foldOrNot("foldunsave")}
          >
            <Icon name="cancel" />
            Clear
          </Form.Field>
        </>
      )
    }
  }


  render() {
    const {isLoading, error, gitbranchestags} = this.props;

    if (error || !gitbranchestags || isLoading || gitbranchestags.length === 0) {
      return (
        <StatusMessage
          error={error || !gitbranchestags}
          // errorClassName="users-error"
          errorMessage={error}
          loading={isLoading}
          loadingMessage={`We are fetching branches & tags for you`}
          nothing={gitbranchestags && gitbranchestags.length === 0}
          nothingMessage={`No branches & tags to display`}
          // nothingClassName="users-error"
          type="default"
        />
      );
    }

    const {
      showmore,
      response,
    } = this.state;

    // ①
    const that = this;

    return (
        <Form>

        {Object.keys(response).map((key, index) => {
          return (
            <Fragment key={index}>
              {/* 每一次type: "LABEL",　就包裹在一个Form.Group中　*/}
              <Form.Group>
                {response[key].map((this_response, i) => (
                  <Fragment key={String(index) + String(i)}>
                    {addfield(that, this_response, key, i)}
                  </Fragment>
                )
                )}
              </Form.Group>
            </Fragment>
          );
        })}










        <Form.Group>
          {/*ｂｅｌｏｗｓ　ａｒｅ　ｂｕｔｔｏｎｓ*/}
          <Form.Field
            control={Button}
            color="green"
            onClick={this.onSubmit}
            // loading={newProjectLoading}
            // disabled={newProjectLoading}
          >
            <Icon name="asexual" />
            Get commits
          </Form.Field>
          {this.showMore(showmore)}
        </Form.Group>
        </Form>

    )
  }
}

const mapStateToProps = state => ({
  isLoading: state.gitBranchesTags.isLoading,
  gitbranchestags: state.gitBranchesTags.gitbranchestags,
  error: state.gitBranchesTags.error,
});

const mapDispatchToProps = dispatch => ({
  fetchGitCommits: (...args) => {
    dispatch(fetchGitCommits(...args));
  },
  fetchGitBranchesTags: (...args) => {
    dispatch(fetchGitBranchesTags(...args));
  },
});

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(CommitsForm);
