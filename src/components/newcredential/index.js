import React, {Component} from 'react';
import {Link} from 'react-router-dom';
import {Form, Input, Icon, Button, Dropdown} from 'semantic-ui-react';

import StatusMessage from '../statusmessage';
import './styles.css';

export default class NewCredential extends Component {
  constructor(props) {
    super(props);
    const {name, username, token, category} = this.props;
    this.state = {
      name,
      username,
      token,
      category,
    };
  }

  componentWillReceiveProps(newProps) {
    const {
      name: newName,
      username: newUsername,
      token: newToken,
      category: newCategory,
    } = newProps;
    this.setState({
      name: newName,
      username: newUsername,
      token: newToken,
      category: newCategory,
    });
  }

  toggleShowEditor = () => {
    this.props.toggleShowEditor();
  };

  onSave = () => {
    // save to redux store (uncontrolled input way)
    const {name, username, token, category} = this.state;
    this.props.updateNewCredential({
      name: name,
      username: username,
      token: token,
      category: category,
    });
    this.toggleShowEditor();
  };

  onCancel = () => {
    // reset & clear everything
    this.setState({
      name: '',
      username: '',
      token: '',
      category: '',
    });
    this.props.updateNewCredential({
      name: '',
      username: '',
      token: '',
      category: '',
    });
    this.toggleShowEditor();
  };

  handleChange = (e, {name, value}) => {
    this.setState({[name]: value});
  };

  isFormValid = () => {
    const {name, username, token, category} = this.state;
    let isFormValid = true;
    if (!name || !username || !token || !category) {
      isFormValid = false;
    }
    return isFormValid;
  };

  onSubmit = () => {
    if (this.isFormValid()) {
      const {name, username, token, category} = this.state;
      const {createCredential} = this.props;
      let newCredential = {
        name: name,
        username: username,
        token: token,
        category: category,
      };
      createCredential(newCredential);
    }
  };

  render() {
    let {
      isLoading,
      success,
      id,
      errorCategory,
      error,
      showEditor,
      categories,
    } = this.props;
    error = error || errorCategory;
    let {name, username, token, category} = this.state;
    const statusMessage = (
      <StatusMessage
        error={error}
        errorClassName="newCredential-message"
        errorMessage={error || 'Oops! Something went wrong.'}
        success={success}
        successClassName="newCredential-message"
        successMessage={
          <Link to={`/credential/${id}`}>
            {'Successful on creating credential'}
          </Link>
        }
        type="modal"
      />
    );

    if (!showEditor) {
      return (
        <div>
          {statusMessage} {/*this will only show the success message*/}
          <div className="newCredential-hidden">
            <Button
              size="small"
              color="blue"
              floated="left"
              onClick={this.toggleShowEditor}>
              <Icon name="edit" />
              New Credential
            </Button>
          </div>
        </div>
      );
    }

    // const categoryTypeDefinitions = [
    //     { key: 'left', text: 'left', value: 'left' },
    //     { key: 'j', text: 'jira', value: 'jira' },
    //     { key: 'g', text: 'gitlab', value: 'gitlab' },
    //     { key: 'right', text: 'right', value: 'right' }
    // ]

    // refer to: https://segmentfault.com/q/1010000015794387
    var categoryTypeDefinitions = categories.map(o => {
      return {key: o.id, text: o.type, value: o.type};
    });

    return (
      <div className="newCredential-show">
        {statusMessage}
        <Form loading={isLoading} className="attached fluid segment">
          <Form.Group widths="equal">
            <Form.Field>
              <label>name</label>
              <Input
                required
                transparent
                type="text"
                name="name"
                value={name}
                onChange={this.handleChange}
                fluid
                placeholder="name"
              />
            </Form.Field>
            <Form.Field>
              <label>username</label>
              <Input
                required
                transparent
                type="text"
                name="username"
                value={username}
                onChange={this.handleChange}
                fluid
                placeholder="username"
              />
            </Form.Field>
            <Form.Field>
              <label>token</label>
              <Input
                required
                transparent
                type="text"
                name="token"
                value={token}
                onChange={this.handleChange}
                fluid
                placeholder="token"
              />
            </Form.Field>
            {/*<Form.Field>*/}
            {/*<label>category</label>*/}
            {/*<Input required transparent type="text" name="category" value={category} onChange={this.handleChange}  fluid placeholder='Input your category (e.g: gitlab jira ...)' />*/}
            {/*</Form.Field>*/}

            {/*dropdown handle way: https://blog.csdn.net/qq_37815596/article/details/84971434*/}
            <Form.Field>
              <label>category</label>
              <Dropdown
                placeholder="Input your category"
                name="category"
                value={category}
                search
                selection
                options={categoryTypeDefinitions}
                onChange={this.handleChange}
              />
            </Form.Field>
          </Form.Group>

          <Button
            color="blue"
            size="small"
            loading={isLoading}
            disabled={isLoading}
            onClick={this.onSubmit}>
            <Icon name="edit" />
            Post redential
          </Button>
          <Button
            color="red"
            role="none"
            size="small"
            disabled={isLoading}
            onClick={this.onSave}>
            <Icon name="save" />
            Save Draft
          </Button>
          <Button
            role="none"
            size="small"
            disabled={isLoading}
            onClick={this.onCancel}>
            <Icon name="cancel" />
            Clear
          </Button>
        </Form>
      </div>
    );
  }
}
