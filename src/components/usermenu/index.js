import React, {Component} from 'react';
import {withRouter} from 'react-router';

import {Dropdown, Image} from 'semantic-ui-react';

class UserMenu extends Component {
  render() {
    const {username, avatar, logout, history, name} = this.props;

    const trigger = (
      <span>
        <Image avatar src={avatar} />
      </span>
    );

    const myProfile = () => {
      history.push(`/user/${username}`);
    };

    const editProfile = () => {
      history.push('/setting');
    };

    const myCredencial = () => {
      history.push('/dashboard/credencial');
    };

    return (
      <Dropdown item trigger={trigger} pointing="top right">
        <Dropdown.Menu>
          <Dropdown.Header icon="user secret" content={name} />
          <Dropdown.Divider />
          <Dropdown.Item onClick={myProfile} icon="user" text="My profile" />
          <Dropdown.Item
            onClick={editProfile}
            icon="pencil"
            text="Edit profile"
          />
          <Dropdown.Item
            onClick={myCredencial}
            icon="lock"
            text="My credentials"
          />
          <Dropdown.Divider />
          <Dropdown.Item onClick={logout} icon="sign out" text="Logout" />
        </Dropdown.Menu>
      </Dropdown>
    );
  }
}

export default withRouter(UserMenu);
