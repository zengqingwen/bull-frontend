import axios from 'axios';
import {GIT_BRANCHTAG_URL} from '../constants';
import {getConfig} from '../../utils/config';

export const fetchGitBranchesTagsApi = (...args) => {
  var config = getConfig()
  config.params = args[0]
  // console.log(config)
  return axios.get(GIT_BRANCHTAG_URL, config);

  // var config = getConfig()
  // config.headers = {...config.headers, ...args[0]}
  // return axios.get(GIT_BRANCHTAG_URL, config);
};
