import {
  DELETE_CREDENTIAL_REQUEST,
  DELETE_CREDENTIAL_SUCCESS,
  DELETE_CREDENTIAL_FAILURE,
  FETCH_CREDENTIAL_REQUEST,
  FETCH_CREDENTIAL_SUCCESS,
  FETCH_CREDENTIAL_FAILURE,
  CREATE_CREDENTIAL_REQUEST,
  CREATE_CREDENTIAL_SUCCESS,
  CREATE_CREDENTIAL_FAILURE,
  CREATE_CREDENTIAL_SAVE,
  CREATE_CREDENTIAL_TOGGLE,
} from './types';
import {
  deleteCredentialApi,
  fetchCredentialApi,
  createCredentialApi,
} from '../api';
import {apiErrorHandler} from '../utils/errorhandler';

export const deleteCredential = id => dispatch => {
  dispatch(deleteCredentialRequest());

  deleteCredentialApi(id)
    .then(response => {
      dispatch(deleteCredentialSuccess());

      // re-load credential page
      fetchCredentialApi(id)
        .then(response => {
          dispatch(fetchCredentialSuccess(response.data));
        })
        .catch(error => {
          const errorMessage = apiErrorHandler(error);
          dispatch(fetchCredentialFailure(errorMessage));
        });
    })
    .catch(error => {
      const errorMessage = apiErrorHandler(error);
      dispatch(deleteCredentialFailure(errorMessage));
    });
};

export const deleteCredentialRequest = () => {
  return {
    type: DELETE_CREDENTIAL_REQUEST,
  };
};

export const deleteCredentialSuccess = () => {
  return {
    type: DELETE_CREDENTIAL_SUCCESS,
  };
};

export const deleteCredentialFailure = error => {
  return {
    type: DELETE_CREDENTIAL_FAILURE,
    error,
  };
};

export const fetchCredential = () => dispatch => {
  dispatch(fetchCredentialRequest());

  fetchCredentialApi()
    .then(response => {
      dispatch(fetchCredentialSuccess(response.data));
    })
    .catch(error => {
      const errorMessage = apiErrorHandler(error);
      dispatch(fetchCredentialFailure(errorMessage));
    });
};

export const fetchCredentialRequest = () => {
  return {
    type: FETCH_CREDENTIAL_REQUEST,
  };
};

export const fetchCredentialSuccess = credential => {
  return {
    type: FETCH_CREDENTIAL_SUCCESS,
    credentials: credential,
  };
};

export const fetchCredentialFailure = error => {
  return {
    type: FETCH_CREDENTIAL_FAILURE,
    error,
  };
};

export const createCredential = newCredential => dispatch => {
  dispatch(createCredentialRequest(newCredential));

  createCredentialApi(newCredential)
    .then(response => {
      dispatch(createCredentialSuccess(response.data));

      // re-load credential page
      fetchCredentialApi()
        .then(response => {
          dispatch(fetchCredentialSuccess(response.data));
        })
        .catch(error => {
          const errorMessage = apiErrorHandler(error);
          dispatch(fetchCredentialFailure(errorMessage));
        });
    })
    .catch(error => {
      const errorMessage = apiErrorHandler(error);
      dispatch(createCredentialFailure(errorMessage));
    });
};

export const createCredentialRequest = newCredential => {
  return {
    type: CREATE_CREDENTIAL_REQUEST,
    newCredential,
  };
};

export const createCredentialSuccess = newCredential => {
  return {
    type: CREATE_CREDENTIAL_SUCCESS,
    newCredential,
  };
};

export const createCredentialFailure = error => {
  return {
    type: CREATE_CREDENTIAL_FAILURE,
    error,
  };
};

export const createCredentialSave = newCredential => {
  return {
    type: CREATE_CREDENTIAL_SAVE,
    name: newCredential.name,
    username: newCredential.username,
    token: newCredential.token,
    category: newCredential.category,
  };
};

export const createCredentialToggle = () => {
  return {
    type: CREATE_CREDENTIAL_TOGGLE,
  };
};
