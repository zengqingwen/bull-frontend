import React, {Component} from 'react';
import {connect} from 'react-redux';
import {fetchProjects, changeFilterScop, fromNavLink} from '../../actions';

import {Container} from 'semantic-ui-react';

import ProjectList from '../../components/projectlist';
import NewProject from '../../components/newproject';

class HomeContainer extends Component {
  componentDidMount() {
    // if(this.props.location.state !== 'fromnavlink') {
    //   this.props.fetchProjects();
    // }
    // 当从navlink　跳到ｈｏｍｅ页面的时候，按照参数来获取。否则默认没有参数获取
    if(this.props.fromnavlink) {
      // console.log("from nav link...")
      const {scop, keyword, sort} = this.props;
      const fromNavLinkParam = {scop, keyword, sort}
      // console.log(fromNavLinkParam)
      this.props.fetchProjects(fromNavLinkParam);
    }else {
      this.props.fetchProjects();
      this.props.changeFilterScop({
        scopInit: true,
        scop: 'yourproject',
        keyword: '',
        sort: '',
      });
    }
    this.props.fromNavLink({fromnavlink: false});
  }

  render() {
    const {isLoading, projects, error, changeFilterScop, scopInit, activeItem, scop, keyword, sort} = this.props;
    return (
      <Container>
        <NewProject
          fetchProjects={fetchProjects}
          changeFilterScop={changeFilterScop}
          scopInit={scopInit}
          activeItem={activeItem}
          scop={scop}
          keyword={keyword}
          sort={sort}
        />
        <ProjectList
          isLoading={isLoading}
          projects={projects}
          error={error}
          // fetchProjects={fetchProjects}
        />
      </Container>
    );
  }
}

const mapStateToProps = state => ({
  isLoading: state.home.isLoading,
  projects: state.home.projects,
  error: state.home.error,
  fromnavlink: state.home.fromnavlink,
  scopInit: state.home.scopInit,
  activeItem: state.home.activeItem,
  scop: state.home.scop,
  keyword: state.home.keyword,
  sort: state.home.sort,
});

const mapDispatchToProps = dispatch => ({
  fetchProjects: (item) => {
    dispatch(fetchProjects(item));
  },
  changeFilterScop: (item) => {
    dispatch(changeFilterScop(item));
  },
  fromNavLink: (item) => {
    dispatch(fromNavLink(item));
  },
});

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(HomeContainer);
