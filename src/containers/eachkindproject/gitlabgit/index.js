import React, {Component} from 'react';

import {Switch, Route} from 'react-router-dom';

import Sidebar from './sidebar';
import TopNav from './topnav';

import Detail from './detail';
import Activity from './activity';
import Commits from './commits';

// import Sidebar from '../../../components/sidebar';
// import TopNav from '../../../components/topnav';
// import Detail from '../../detail';

class GitlabGitContainer extends Component {
  render() {
    // console.log("1111111111111")
    // console.log(this.props.projectInfo)
    const {isToggled, projectInfo} = this.props;

    return (
      <div id="dashboard">
        <div id="main" className={isToggled ? 'toggled' : ''}>
          <TopNav />
          {/*<br /><br /><br /><div>{projectInfo.cate_type} - This is public area, but have no use.</div>*/}
          <Switch>
            {/*<Route exact path="/:namespace/:prjectname/activity" component={Activity} />*/}
            <Route exact path="/:namespace/:prjectname/activity" render={(props)=><Activity {...props} projectinfo={projectInfo}/>} />
            {/*<Route exact path="/:namespace/:prjectname/commits" component={Commits} />*/}
            {/*讲道理，只有Commits　模块中使用了projectinfo才使用以下；如果没使用projectinfo，则用以上写法即可；但为了保险期间，都写成以下传递projectinfo总没问题*/}
            <Route exact path="/:namespace/:prjectname/commits" render={(props)=><Commits {...props} projectinfo={projectInfo}/>} />
            {/*<Route exact path="/:namespace/:prjectname/" component={Detail} />*/}
            <Route exact path="/:namespace/:prjectname/" render={(props)=><Detail {...props} projectinfo={projectInfo}/>} />
          </Switch>
        </div>
        <Sidebar
          projectInfo={projectInfo}
        />
      </div>
    )
  }
}

export default GitlabGitContainer;
