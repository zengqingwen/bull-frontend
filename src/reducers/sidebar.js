import {TOGGLE_SIDEBAR} from '../actions/types';

function navToggleReducer(state = true, {type}) {
  switch (type) {
    case TOGGLE_SIDEBAR:
      return !state;
    default:
      return state;
  }
}

export default navToggleReducer;
