import {
  FETCH_CREDENTIAL_REQUEST,
  FETCH_CREDENTIAL_SUCCESS,
  FETCH_CREDENTIAL_FAILURE,
  DELETE_CREDENTIAL_REQUEST,
  DELETE_CREDENTIAL_SUCCESS,
  DELETE_CREDENTIAL_FAILURE,
  CREATE_CREDENTIAL_REQUEST,
  CREATE_CREDENTIAL_SUCCESS,
  CREATE_CREDENTIAL_FAILURE,
  CREATE_CREDENTIAL_SAVE,
  CREATE_CREDENTIAL_TOGGLE,
  LOGOUT,
} from '../actions/types';

const credentialInitialState = {
  isLoading: false,
  credentials: null,
  error: null,
};

const newCredentialInitialState = {
  newCredentialLoading: false,
  newCredentialSuccess: false,

  newCredentialName: '',
  newCredentialUsername: '',
  newCredentialToken: '',
  newCredentialCategory: '',
  newCredentialId: null,

  newCredentialError: null,
  newCredentialShow: false,
};

const initialState = {
  ...credentialInitialState,
  ...newCredentialInitialState,
};

const credential = (state = initialState, action) => {
  switch (action.type) {
    case FETCH_CREDENTIAL_REQUEST:
      return {
        ...credentialInitialState,
        ...state,
        newCredentialLoading: false,
        newCredentialSuccess: false,
        newCredentialId: null,
        newCredentialError: null,
        newCredentialShow: false,
        isLoading: true,
        error: null,
      };
    case FETCH_CREDENTIAL_SUCCESS:
      return {
        ...state,
        isLoading: false,
        credentials: action.credentials,
        error: null,
      };
    case FETCH_CREDENTIAL_FAILURE:
      return {
        ...state,
        error: action.error,
      };

    case DELETE_CREDENTIAL_REQUEST:
      return {
        ...state,
        isDeleting: true,
        deleteError: null,
      };
    case DELETE_CREDENTIAL_SUCCESS:
      return {
        ...state,
        isDeleting: false,
        deleteError: null,
      };
    case DELETE_CREDENTIAL_FAILURE:
      return {
        ...state,
        isDeleting: false,
        deleteError: action.error,
      };

    case CREATE_CREDENTIAL_REQUEST:
      return {
        ...state,
        newCredentialLoading: true,
        newCredentialSuccess: false,
        newCredentialError: null,
        newCredentialName: action.newCredential.name,
        newCredentialUsername: action.newCredential.username,
        newCredentialToken: action.newCredential.token,
        newCredentialCategory: action.newCredential.category,
      };
    case CREATE_CREDENTIAL_SUCCESS:
      return {
        ...state,
        newCredentialLoading: false,
        newCredentialSuccess: true,
        newCredentialName: '',
        newCredentialUsername: '',
        newCredentialToken: '',
        newCredentialCategory: '',
        newCredentialId: action.newCredential.id,
        newCredentialShow: false,
        newCredentialError: null,
      };
    case CREATE_CREDENTIAL_FAILURE:
      return {
        ...state,
        newCredentialLoading: false,
        newCredentialSuccess: false,
        newCredentialId: null,
        newCredentialShow: true,
        newCredentialError: action.error,
      };
    case CREATE_CREDENTIAL_SAVE:
      return {
        ...state,
        newCredentialName: action.name,
        newCredentialUsername: action.username,
        newCredentialToken: action.token,
        newCredentialCategory: action.category,
      };
    case CREATE_CREDENTIAL_TOGGLE:
      return {
        ...state,
        newCredentialShow: !state.newCredentialShow,
        newCredentialSuccess: false,
        newCredentialError: null,
      };
    case LOGOUT:
      return {
        ...state,
        ...newCredentialInitialState,
      };
    default:
      return state;
  }
};

export default credential;
