import React, {Component} from 'react';
import PropTypes from 'prop-types';
import {connect} from 'react-redux';

import {fetchProjectByName, initialFetchProjectByName} from '../../actions';

import StatusMessage from '../../components/statusmessage';

import GitlabContainer from '../eachkindproject/gitlabcommon';
import GitlabGitContainer from '../eachkindproject/gitlabgit';
import JenkinsContainer from '../eachkindproject/jenkinscommon';


import './styles.css';

// const ProjectContainer = ({isToggled}) => (
//   <div id="dashboard">
//     <div id="main" className={isToggled ? 'toggled' : ''}>
//       <div>{this.props.projectinfo.name}</div>
//       <TopNav />
//       <Switch>
//         {/* <Route exact path='33' component={Detail} /> */}
//         {/* OR */}
//         <Route exact path="/:namespace/:prjectname/33" component={Detail} />
//       </Switch>
//     </div>
//     <Sidebar />
//   </div>
// );


class ProjectContainer extends Component {
  componentWillMount() {
    this.props.initialFetchProjectByName();

    // var projectinfo = this.props.location.state.hasOwnProperty('projectinfo');
    var state_projectinfo = this.props.location.state;
    if(!state_projectinfo) {
      var namespace = this.props.match.params.namespace;
      var prjectname = this.props.match.params.prjectname;
      this.props.fetchProjectByName(namespace, prjectname);
    }
  }

  render() {
    const { isToggled } = this.props


    let state_projectinfo = this.props.location.state;
    var projectinfo
    if(state_projectinfo) {
      console.log("I do not fetch");
      projectinfo = state_projectinfo.projectinfo
    }else {
      console.log("I fetching...");

      const { fetchedProjectByNameLoading, fetchedProjectByNameError, fetchedProjectByName } = this.props
      if(fetchedProjectByNameError || fetchedProjectByNameLoading) {
        return (
          <StatusMessage
            error={fetchedProjectByNameError}
            errorClassName="forum-error"
            errorMessage={fetchedProjectByNameError}
            loading={fetchedProjectByNameLoading}
            loadingMessage={`We are fetching the project for you`}
            // nothing={threads && threads.length === 0}
            // nothingMessage={`No threads to display`}
            // nothingClassName="forum-error"
            type="default"
          />
        )
      }

      projectinfo = fetchedProjectByName
    }
    // console.log("#########");
    // console.log(projectinfo);
    if(projectinfo.cate_type === 'gitlab') {
      console.log("#gitlab#");
      return (
        <GitlabContainer
          isToggled={isToggled}
          projectInfo={projectinfo}
        />
      )
    }else if(projectinfo.cate_type === 'gitlab-git') {
      console.log("#gitlab-git#");
      return (
        <GitlabGitContainer
          isToggled={isToggled}
          projectInfo={projectinfo}
        />
      )
    }else {
      console.log("#others#");
      return (
        <JenkinsContainer
          isToggled={isToggled}
          projectInfo={projectinfo}
        />
      )
    }

  }
}

ProjectContainer.propTypes = {
  isToggled: PropTypes.bool.isRequired,
};

const mapStateToProps = state => ({
  isToggled: state.isSidebarToggled,

  fetchedProjectByNameLoading: state.project.fetchedProjectByNameLoading,
  fetchedProjectByNameSuccess: state.project.fetchedProjectByNameSuccess,
  fetchedProjectByNameError: state.project.fetchedProjectByNameError,
  fetchedProjectByName: state.project.fetchedProjectByName,
});

const mapDispatchToProps = dispatch => ({
  fetchProjectByName: (namespace, prjectname) => {
    dispatch(fetchProjectByName(namespace, prjectname))
  },
  initialFetchProjectByName: () => {
    dispatch(initialFetchProjectByName())
  },
});

export default connect(mapStateToProps, mapDispatchToProps)(ProjectContainer);

// class ProjectContainer extends Component {
//   render() {
//     return (
//       <div id="dashboard">
//         <div id="main" className={'isToggled' ? 'toggled' : ''}>
//           <TopNav />
//           {/* <Switch>
//             <Route exact path='/' component={Inbox} />
//             <Route path={`${home}/email/:id`} component={Email} />
//           </Switch> */}

//         </div>

//         <Sidebar />
//       </div>
//     );
//   }
// }

// export default ProjectContainer;
