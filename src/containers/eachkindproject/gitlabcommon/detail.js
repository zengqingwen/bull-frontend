import React, {Component} from 'react';
import {Segment} from 'semantic-ui-react';

import './styles.css';

class Detail extends Component {
  render() {
    return (
      <Segment basic id="content">
        <h1>Details</h1>
      </Segment>
    );
  }
}

export default Detail;