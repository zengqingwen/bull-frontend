import {
  INITIAL_CREATE_PROJECTPAGE,
  CREATE_PROJECT_REQUEST,
  CREATE_PROJECT_SUCCESS,
  CREATE_PROJECT_FAILURE,

  INITIAL_FETCH_PROJECT_BY_NAME,
  FETCH_PROJECT_BY_NAME_REQUEST,
  FETCH_PROJECT_BY_NAME_SUCCESS,
  FETCH_PROJECT_BY_NAME_FAILURE,
} from '../actions/types';

const newProjectInitialState = {
  newProjectLoading: false,
  newProjectSuccess: false,
  newProjectError: null,

  newProject: null,
};

const fetchedProjectByNameInitialState = {
  fetchedProjectByNameLoading: false,
  fetchedProjectByNameSuccess: false,
  fetchedProjectByNameError: null,

  fetchedProjectByName: null,
};

const initialState = {
  ...newProjectInitialState,
  ...fetchedProjectByNameInitialState,
};

const project = (state = initialState, action) => {
  switch (action.type) {
    case INITIAL_CREATE_PROJECTPAGE:
      return {
        ...state,
        newProjectLoading: false,
        newProjectSuccess: false,
        newProjectError: null,
      };
    case CREATE_PROJECT_REQUEST:
      return {
        ...state,
        newProjectLoading: true,
        newProjectSuccess: false,
        newProjectError: null,
      };
    case CREATE_PROJECT_SUCCESS:
      return {
        ...state,
        newProjectLoading: false,
        newProjectSuccess: true,

        newProject: action.newProject,

        newProjectError: null,
      };
    case CREATE_PROJECT_FAILURE:
      return {
        ...state,
        newProjectLoading: false,
        newProjectSuccess: false,
        newProject: null,

        newProjectError: action.error,
      };




    case INITIAL_FETCH_PROJECT_BY_NAME:
      return {
        ...state,
        fetchedProjectByNameLoading: false,
        fetchedProjectByNameSuccess: false,
        fetchedProjectByNameError: null,
      };
    case FETCH_PROJECT_BY_NAME_REQUEST:
      return {
        ...state,
        fetchedProjectByNameLoading: true,
        fetchedProjectByNameSuccess: false,
        fetchedProjectByNameError: null,
      };
    case FETCH_PROJECT_BY_NAME_SUCCESS:
      return {
        ...state,
        fetchedProjectByNameLoading: false,
        fetchedProjectByNameSuccess: true,
        fetchedProjectByName: action.project,
        fetchedProjectByNameError: null,
      };
    case FETCH_PROJECT_BY_NAME_FAILURE:
      return {
        ...state,
        fetchedProjectByNameLoading: false,
        fetchedProjectByNameSuccess: false,
        fetchedProjectByName: null,
        fetchedProjectByNameError: action.error,
      };
    default:
      return state;
  }
};

export default project;
