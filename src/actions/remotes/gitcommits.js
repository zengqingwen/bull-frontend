import {
  INITIAL_GIT_COMMITS,
  FETCH_GIT_COMMITS_REQUEST,
  FETCH_GIT_COMMITS_SUCCESS,
  FETCH_GIT_COMMITS_FAILURE,
} from '../types';

import {
  fetchGitCommitsApi,
} from '../../api/index';
import {apiErrorHandler} from '../../utils/errorhandler';

export const initialGitCommits = () => {
  console.log("ttttttttttttttttttttyyyyyyyyyyyyy")
  return {
    type: INITIAL_GIT_COMMITS,
  }
}

export const fetchGitCommits = (...args) => dispatch => {
  dispatch(fetchGitCommitsRequest());

  fetchGitCommitsApi(...args)
    .then(response => {
      dispatch(fetchGitCommitsSuccess(response.data));
    })
    .catch(error => {
      const errorMessage = apiErrorHandler(error);
      dispatch(fetchGitCommitsFailure(errorMessage));
    });
};

export const fetchGitCommitsRequest = () => {
  return {
    type: FETCH_GIT_COMMITS_REQUEST,
  };
};

export const fetchGitCommitsSuccess = gitcommits => {
  return {
    type: FETCH_GIT_COMMITS_SUCCESS,
    gitcommits: gitcommits,
  };
};

export const fetchGitCommitsFailure = error => {
  return {
    type: FETCH_GIT_COMMITS_FAILURE,
    error,
  };
};
