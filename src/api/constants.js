export const API_URL =
  process.env.NODE_ENV === 'production'
    ? process.env.REACT_APP_PROD_API_URL
    : process.env.REACT_APP_DEV_API_URL;

export const USER_URL = API_URL + 'user/';
export const USER_LOGIN_URL = USER_URL + 'login/';
export const USER_LOGOUT_URL = USER_URL + 'logout/';
export const USER_REGISTER_URL = USER_URL + 'register/';

export const USER_EDIT_URL = '/edit/';

// project url
// export const PROJECT_URL = API_URL + 'project/';
//
// export const CATEGORY_URL = PROJECT_URL + 'category/';
// export const CREDENTIAL_URL = PROJECT_URL + 'credential/';

export const CATEGORY_URL = API_URL + 'categories/';
export const CREDENTIAL_URL = API_URL + 'credentials/';

export const PROJECT_URL = API_URL + 'projects/';

export const FETCH_PROJECT_BY_NAME_URL = API_URL + 'retriveprojectbyname/';

export const GIT_COMMIT_URL = API_URL + 'remote/gitlab-git/init-or-fetch-commits/';
export const GIT_BRANCHTAG_URL = API_URL + 'remote/gitlab-git/init-or-fetch-branchestags/';
