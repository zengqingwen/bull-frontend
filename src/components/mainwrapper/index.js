import React, {Component} from 'react';

class MainWrapper extends Component {
  // constructor() {
  //   super()
  //   // Bind the method to the component context
  //   this.renderChildren = this.renderChildren.bind(this)
  // }

  // renderChildren() {
  //   // TODO: Change the name prop of all children
  //   // to this.props.name
  //   return this.props.children
  // }

  render() {
    return (
      <div id="main">
        {/* {this.renderChildren()} */}
        {this.props.children}
        {/* aaaaa */}
      </div>
    );

    // if(this.props.children._owner.type.name === 'ProjectContainer') {
    //   return (
    //     this.props.children
    //   )
    // }else{
    //   return (
    //   <div id="main">
    //     {/* {this.renderChildren()} */}
    //     {this.props.children}
    //     aaaaa
    //   </div>
    //   )
    // }
  }
}

export default MainWrapper;
