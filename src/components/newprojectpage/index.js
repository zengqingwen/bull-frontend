import React, {Component} from 'react';
import {withRouter} from 'react-router';

import {
  Form,
  Header,
  Input,
  Radio,
  Select,
  TextArea,
  Button,
} from 'semantic-ui-react';
import StatusMessage from '../statusmessage';

import './styles.css';

// const options = [
//     { key: 'm', text: 'Male', value: 'male' },
//     { key: 'f', text: 'Female', value: 'female' },
//     { key: 'o', text: 'Other', value: 'other' },
// ]

class NewProjectPage extends Component {
  constructor(props) {
    super(props);
    //　①
    this.state = {
      credentialDefinitions: [],
      name: '',
      origin_url: '',
      origin_jack_name: '',
      origin_third: '',
      origin_forth: '',
      visible: '1',
      category: '',
      credential: '',
      remark: '',
      newProject: null,
    };
  }

  componentWillReceiveProps(newProps) {
    if(newProps.newProjectSuccess) {
      this.props.history.push({
        pathname: `/${newProps.newProject.owner}/${newProps.newProject.name}`,　state: {projectinfo: newProps.newProject},
      });
    }
  }

  handleChange = (e, {name, value}) => {
    this.setState({[name]: value});
  };

  // category_widget = (<div>this is fenjiexian</div>);

  // 讲道理，前台在此处过滤了，后台也应该过滤！！！
  handleChangePlus = (e, {name, value}) => {
    this.setState({[name]: value});

    var credentials = this.props.credentials;
    var this_cate_credentials = [];
    var your_choose_category = value.split('-')[0];
    for (var credential in credentials) {
      let cred = credentials[credential];
      if (cred.cate.split('-')[0] === your_choose_category) {
        this_cate_credentials.push(cred);
      }
    }
    var credentialDefinitions = this_cate_credentials.map(o => {
      return {key: o.id, text: o.name, value: o.name};
    });
    this.setState({credentialDefinitions: credentialDefinitions});

    //　①
    // 由于category 控制了以下控件，故每次category改变时，以下控件的内容也要置空
    //　这块不加如下代码就是个ｂｕｇ
    this.setState({
      origin_url: '',
      origin_jack_name: '',
      origin_third: '',
      origin_forth: '',
      // 讲道理credential　不用置空也可以，好像会自动重新选择置空，但是事实却不是这样，主要是this.state.credential的值变化了，然后没能自动清空
      credential: '',
    });
  };

  isFormValid = () => {
    //　①
    const {name, visible, category} = this.state;
    let isFormValid = true;
    if (!name || !visible || !category) {
      isFormValid = false;
    }
    return isFormValid;
  };

  onSubmit = () => {
    if (this.isFormValid()) {
      const {
        name,
        origin_url,
        origin_jack_name,
        //　①
        origin_third,
        origin_forth,
        visible,
        category,
        credential,
        remark,
      } = this.state;
      const {createProject} = this.props;
      let newProjectForm = {
        name: name,
        origin_url: origin_url,
        origin_jack_name: origin_jack_name,
        //　①
        origin_third: origin_third,
        origin_forth: origin_forth,
        visible: visible,
        category: category,
        credential: credential,
        remark: remark,
      };
      // 创建成功后要跳转页面，所以就不在置空state了
      createProject(newProjectForm);

      // 这个需要放到componentWillReceiveProps生命周期函数中
      // const {newProject} = this.props;
      // if (newProject) {
      //   // this.props.history.push('/aaa/bbb')
      //   // https://blog.csdn.net/baidu_38027860/article/details/82888974    跳转页面组件间传值
      //   // 父组件：　this.props.history.push({ pathname: "/PutForwardSubmit", state: { vcode } });
      //   // 子组件取值：　this.props.location.state.vcode
      //   this.props.history.push({
      //     pathname: `/${newProject.owner}/${newProject.name}`,
      //     state: {projectinfo: newProject},
      //   });
      // }
    }
  };

  render() {
    let {
      // isLoading,
      categories,
      error,
      success,
      username,
      newProjectLoading,
      newProjectSuccess,
      newProjectError,
    } = this.props;
    //　①
    let {
      name,
      origin_url,
      origin_jack_name,
      origin_third,
      origin_forth,
      visible,
      category,
      credential,
      remark,
    } = this.state;

    error = error || newProjectError;
    success = success || newProjectSuccess;

    const statusMessage = (
      <StatusMessage
        error={error}
        errorClassName="newProject-message"
        errorMessage={error || 'Oops! Something went wrong.'}
        success={success}
        successClassName="newProject-message"
        successMessage="Successful on creating credential"
        type="modal"
      />
    );

    var categoryTypeDefinitions = categories.map(o => {
      return {key: o.id, text: o.type, value: o.type};
    });

    var {credentialDefinitions} = this.state;

    //　①
    const category_base = category.split('-')[0]
    const origin_url_widget = (
      <Form.Input
        required
        label={`${category_base}对接地址`}
        name="origin_url"
        value={origin_url}
        onChange={this.handleChange}
        placeholder={`${category_base}对接地址`}
        width={8}
      />
    );
    const origin_jack_name_widget = (
      <Form.Input
        required
        label={`${category_base}对接工程名`}
        name="origin_jack_name"
        value={origin_jack_name}
        onChange={this.handleChange}
        placeholder={`${category_base}对接工程名`}
        width={8}
      />
    );
    // 第三个扩展控件（箭头函数　＋　自执行函数）
    const origin_third_widget = (() => {
      let this_label = category_base
      if(category_base === 'gitlab') {
        this_label += ' namespace'
      }else {
        this_label += ' origin_third'
      }
      return (
        <Form.Input
          required
          label={this_label}
          name="origin_third"
          value={origin_third}
          onChange={this.handleChange}
          placeholder={this_label}
          width={8}
        />
      )
    })();
    // 第四个扩展控件（箭头函数　＋　自执行函数）
    const origin_forth_widget = (() => {
      let this_label = category_base + ' origin_forth'
      return (
        <Form.Input
          required
          label={this_label}
          name="origin_forth"
          value={origin_forth}
          onChange={this.handleChange}
          placeholder={this_label}
          width={8}
        />
      )
    })();

    //　①　判断按照各个category拼接不同的widgets组合
    var category_widget
    if(category_base === 'gitlab') {
      category_widget = (
        <>
          <Form.Group>
            {origin_url_widget}
          </Form.Group>
          <Form.Group>
            {origin_third_widget}
            {origin_jack_name_widget}
          </Form.Group>
        </>
      );
    }else if(category_base === 'jenkins') {
      category_widget = (
        <Form.Group>
          {origin_url_widget}
          {origin_jack_name_widget}
        </Form.Group>
      );
    }else if(category_base === 'XXX') {
      // 只是为了让第四个备用控件有个地方放着
      category_widget = (
        <Form.Group>
          {origin_forth_widget}
        </Form.Group>
      );
    }else {
      category_widget = (
        <Form.Group>
          {origin_url_widget}
        </Form.Group>
      );
    }


    return (
      <div>
        <Header as="h3" content="New project" textAlign="center" />
        {statusMessage}
        <Form>
          <Form.Group>
            <Form.Input label="Project path" width={6}>
              <Input label="http://localhost/" placeholder={username} disabled/>
            </Form.Input>
            <Form.Input
              required
              label="Project name"
              name="name"
              value={name}
              onChange={this.handleChange}
              placeholder="Project Name"
              width={10}
            />
          </Form.Group>

          {/*<Form.Group>*/}
            {/*<Form.Input*/}
              {/*required*/}
              {/*label="对接地址"*/}
              {/*name="origin_url"*/}
              {/*value={origin_url}*/}
              {/*onChange={this.handleChange}*/}
              {/*placeholder="对接地址"*/}
              {/*width={8}*/}
            {/*/>*/}
            {/*<Form.Input*/}
              {/*required*/}
              {/*label="对接工程名"*/}
              {/*name="origin_jack_name"*/}
              {/*value={origin_jack_name}*/}
              {/*onChange={this.handleChange}*/}
              {/*placeholder="对接工程名"*/}
              {/*width={8}*/}
            {/*/>*/}
          {/*</Form.Group>*/}

          <Form.TextArea
            control={TextArea}
            label="Project description(optional)"
            name="remark"
            value={remark}
            onChange={this.handleChange}
            placeholder="Description format"
          />

          <Form.Group grouped>
            <label>Visibility Level</label>
            <Form.Field
              label="Public"
              control={Radio}
              type="radio"
              name="visible"
              value="1"
              checked={visible === '1'}
              onChange={this.handleChange}
            />
            <Form.Field
              label="Internal"
              control={Radio}
              type="radio"
              name="visible"
              value="2"
              checked={visible === '2'}
              onChange={this.handleChange}
            />
            <Form.Field
              label="Private"
              control={Radio}
              type="radio"
              name="visible"
              value="3"
              checked={visible === '3'}
              onChange={this.handleChange}
            />
          </Form.Group>

          <Form.Group>
            <Form.Field
              required
              control={Select}
              label="Category"
              search
              name="category"
              value={category}
              options={categoryTypeDefinitions}
              onChange={this.handleChangePlus}
              placeholder="Category"
              width={8}
            />
            <Form.Field
              control={Select}
              label="Credential"
              search
              name="credential"
              value={credential}
              options={credentialDefinitions}
              onChange={this.handleChange}
              placeholder="Credential"
              width={8}
            />
          </Form.Group>

          {category_widget}

          <Form.Field
            control={Button}
            color="green"
            onClick={this.onSubmit}
            loading={newProjectLoading}
            disabled={newProjectLoading}>
            Create project
          </Form.Field>
        </Form>
      </div>
    );
  }
}

export default withRouter(NewProjectPage);
