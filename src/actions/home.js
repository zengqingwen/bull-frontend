import {
  FETCH_HOME_REQUEST,
  FETCH_HOME_SUCCESS,
  FETCH_HOME_FAILURE,
  CHANG_EFILTER_SCOP,
  FROM_NAV_LINK,
} from './types';
import {fetchProjectsApi} from '../api';
import {apiErrorHandler} from '../utils/errorhandler';

export const fetchProjects = (...args) => dispatch => {
  dispatch(fetchHomeRequest());

  fetchProjectsApi(...args)
    .then(response => {
      dispatch(fetchHomeSuccess(response.data));
    })
    .catch(error => {
      const errorMessage = apiErrorHandler(error);
      dispatch(fetchHomeFailure(errorMessage));
    });
};

export const fetchHomeRequest = () => {
  return {
    type: FETCH_HOME_REQUEST,
  };
};

export const fetchHomeSuccess = data => {
  return {
    type: FETCH_HOME_SUCCESS,
    projects: data,
  };
};

export const fetchHomeFailure = error => {
  return {
    type: FETCH_HOME_FAILURE,
    error,
  };
};

export const changeFilterScop = item => {
  return {
    type: CHANG_EFILTER_SCOP,
    item,
  }
};

export const fromNavLink = (item) => {
  return {
    type: FROM_NAV_LINK,
    item,
  }
};
