import React from 'react';

import {
  Form,
  // Header,
  // Input,
  // Radio,
  Select,
  Header,
  Label,
  Radio,
  // Button,
  // Divider,
  // Label,
} from 'semantic-ui-react';

import DatePicker from "react-datepicker";

import moment from "moment";


export const getFormatedData = (data) => {
  const obj = {};
  var lastLabel = "";
  for (var i in data) {
    if (data[i].type === "LABEL") {
      obj[i] = [];
      lastLabel = i;
    } else {
      data[i].displaytext = i;
      if (data[i].dataType === "DATE") {
        if (data[i].value) {
          console.log(data[i].value);
          const dateSplit = data[i].value.split("-");
          const objDate = new Date(
            dateSplit[1] + " " + dateSplit[0] + ", " + dateSplit[2]
          );
          data[i].value = new Date(+moment(objDate).format("x"));
        } else {
          data[i].value = null;
        }
      }
      obj[lastLabel] = [...obj[lastLabel], data[i]];
    }
  }
  return obj;
}


// for DATE only
const changeDate = (that, event, key, index) => {
  const { response } = that.state;
  const obj = { ...response[key][index] };

  obj.value = event;
  response[key][index] = obj;

  that.setState({
    response
  });
};

// for SINGLECHECKBOX
const handleCheck = (that, event, {checked}, key, index) => {
  // console.log('this.state.checkboxGroup')
  // console.log(event.target.checked)
  // console.log(event)
  // console.log(checked)
  const { response } = that.state;
  const obj = { ...response[key][index] };

  obj.checked = !checked;
  response[key][index] = obj;

  that.setState({
    response
  });
}

// for INPUT &
const changeValue = (that, event, key, index) => {
  const { response } = that.state;
  const obj = { ...response[key][index] };

  obj.value = event.target.value;
  response[key][index] = obj;

  that.setState({
    response
  });
};

// for RADIO    该（changeItem）跟以下（handleChangeSelect）两个方法非常相像了
const changeItem = (that, event, value, key, index) => {
  const { response } = that.state;
  const obj = { ...response[key][index] };

  obj.value = value;
  response[key][index] = obj;

  that.setState({
    response
  });
};

// for SELECTINPUT
const handleChangeSelect = (that, event, {value}, key, index) => {
  const { response } = that.state;
  const obj = { ...response[key][index] };

  if(obj.name === "branches") {
    response[key][2].value = "";
    response[key][3].value = "";
  }else if(obj.name === "startPoint" || obj.name === "endPoint") {
    response[key][0].value = [];
  }

  obj.value = value;
  response[key][index] = obj;

  that.setState({
    response
  });
};
// for SELECTINPUT
const handleAdditionSelect = (that, event, {value}, key, index) => {
  const { response } = that.state;
  const obj = { ...response[key][index] };

  obj.options = [{ text: value, value }, ...obj.options];
  response[key][index] = obj;

  that.setState({
    response
  });
}

// addfield(dataType, value, placehoder, key, i) {
export const addfield = (that, this_response, key, i) => {
  console.log(this_response.dataType);
  // const {displaytext} = this_response;
  const {dataType, styleclass, required, label, name, value, placeholder, size, width} = this_response;
  switch (dataType) {
    case "DATE":
      return (
        <div style={{padding:'6px'}} className={styleclass}>
          <Header sub>{label}</Header>
          <DatePicker
            selected={value}
            onChange={e => changeDate(that, e, key, i)}
            dateFormat="YYYY-MM-dd"
            name={name}
          />
        </div>
      );
    // break;
    case "SELECTINPUT": // 包括多选和单选（ｓｅｌｅｃｔ）
      const {options, multiple} = this_response;
      return (
        <Form.Field
          control={Select}
          className={styleclass}
          required={required}
          multiple={multiple}
          options={options}
          value={value}
          label={label}
          name={name}
          placeholder={placeholder}
          size={size}
          width={width}
          search
          selection
          fluid
          clearable
          allowAdditions
          onChange={(e, {value}) => handleChangeSelect(that, e, {value}, key, i)}
          onAddItem={(e, {value}) => handleAdditionSelect(that, e, {value}, key, i)}
        />
      );
    // break;
    case "INPUT":
      return (
        <Form.Input
          className={styleclass}
          required={required}
          value={value}
          // label={displaytext.replace(/\_/g, " ")}
          label={label}
          name={name}
          placeholder={placeholder}
          size={size}
          width={width}
          onChange={e => changeValue(that, e, key, i)}
        />
      );
    // break;
    case "SINGLECHECKBOX":
      const {checked, disabled} = this_response;
      return (
        <Form.Checkbox
          className={styleclass}
          label={label}
          checked={checked}
          name={name}
          disabled={disabled}
          onChange={e => handleCheck(that, e, {checked}, key, i)}
          width={width}
        />
      );
    // break;
    case "RADIO":
      const {itemchoices} = this_response;
      return (
        <div className={styleclass}>
          <label>{label}</label>
          {Object.keys(itemchoices).map((k) => (
              <Form.Field
                key={k}
                label={k}
                control={Radio}
                type="radio"
                name={name}
                // value={itemchoices[k]}
                checked={itemchoices[k] === value}
                onChange={e => changeItem(that, e, itemchoices[k], key, i)}
              />
          ))}
        </div>
      );
    // break;
    default:
      const {circular, color} = this_response;
      return (
        <Form.Field className={styleclass} control={Label} label={label} circular={circular} color={color}>or</Form.Field>
      );
    // break;
  }
}