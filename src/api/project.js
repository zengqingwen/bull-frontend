import axios from 'axios';
import {PROJECT_URL, FETCH_PROJECT_BY_NAME_URL} from './constants';
import {getConfig} from '../utils/config';

export const fetchProjectsApi = (...args) => {
  if(args.length) {
    // console.log("balalala11")
    var config = getConfig()
    config.params = args[0]
    return axios.get(PROJECT_URL, config);
  }else {
    // console.log("balalala22")
    return axios.get(PROJECT_URL, getConfig());
  }
};

// export const fetchProjectApi = project => {
//     return axios.get(PROJECT_URL + project, getConfig());
// };
export const fetchProjectByNameApi = (namespace, prjectname) => {
    return axios.get(FETCH_PROJECT_BY_NAME_URL + namespace + '/' + prjectname, getConfig());
};

export const createProjectApi = newProject => {
  return axios.post(PROJECT_URL, newProject, getConfig());
};
