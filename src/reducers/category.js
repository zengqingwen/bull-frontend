import {
  FETCH_CATEGORY_REQUEST,
  FETCH_CATEGORY_SUCCESS,
  FETCH_CATEGORY_FAILURE,
} from '../actions/types';

const initialState = {
  isLoading: false,
  categories: null,
  error: null,
};

const category = (state = initialState, action) => {
  switch (action.type) {
    case FETCH_CATEGORY_REQUEST:
      return {
        ...state,
        isLoading: true,
        error: null,
      };
    case FETCH_CATEGORY_SUCCESS:
      return {
        ...state,
        isLoading: false,
        categories: action.categories,
        error: null,
      };
    case FETCH_CATEGORY_FAILURE:
      return {
        ...state,
        error: action.error,
      };
    default:
      return state;
  }
};

export default category;
